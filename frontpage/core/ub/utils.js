/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

 OpenBlock.Utils = {
    makeSN() {
        let r = Math.random() * Number.MAX_SAFE_INTEGER;
        let sn = Date.now().toString(36) + "-" + Math.floor(r).toString(36);
        return sn;
    },
    hasName(arr, name) {
        for (let a of arr) {
            if (a.name === name) {
                return true;
            }
        }
        return false;
    },

    genName(base, checkArr) {
        let postfix = 1, newname = base;
        while (OpenBlock.Utils.hasName(checkArr, newname)) {
            newname = `${base}_${postfix++}`;
        }
        return newname;
    },
    eventsSkippedSaving:[
        Blockly.Events.UI,
        Blockly.Events.BLOCK_DRAG,
        Blockly.Events.SELECTED,
        Blockly.Events.CLICK,
        Blockly.Events.BUBBLE_OPEN,
        Blockly.Events.TRASHCAN_OPEN,
        Blockly.Events.TOOLBOX_ITEM_SELECT,
        Blockly.Events.THEME_CHANGE,
        Blockly.Events.VIEWPORT_CHANGE,
    ],
    
};