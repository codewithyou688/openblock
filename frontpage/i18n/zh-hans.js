// This file was automatically generated.  Do not modify.

/*jshint -W117 */
Blockly.Msg["MESSAGE"] = "消息";
Blockly.Msg["FSM_CTRL"] = "状态机控制";
Blockly.Msg["EVENT"] = "事件";
Blockly.Msg["DEBUG"] = "调试";
Blockly.Msg["TEXT"] = "文字";
Blockly.Msg["BOOLEAN"] = "布尔";
Blockly.Msg["LOOPS"] = "循环";
Blockly.Msg["MATH"] = "数学";
Blockly.Msg["LISTS"] = "列表";
Blockly.Msg["STRUCTS"] = "数据结构";
Blockly.Msg["METHOD"] = "函数";
Blockly.Msg["VAR_FSM"] = "状态机变量";
Blockly.Msg["VAR_STATE"] = "状态变量";
Blockly.Msg["VAR_LOCAL"] = "局部变量";

//blockly原版

Blockly.Msg["TEXT_GET_SUBSTRING_END_FROM_END"] = "到倒数第#个字符";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDDOWN"] = "向下取整";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDUP"] = "向上取整";
Blockly.Msg["MATH_RANDOM_INT_TITLE"] = "随机整数[%1,%2]";
// Blockly.Msg["Number"]="数字";
// Blockly.Msg["String"]="字符串";
// Blockly.Msg["Boolean"]="布尔";
// Blockly.Msg["Integer"]="整数";
// Blockly.Msg["FSM"]="状态机";
Blockly.Msg["title"] = "标题";
Blockly.Msg["object"] = "结构体数据";
Blockly.Msg["name"] = "名字";
Blockly.Msg["KeyCode"]="按键码";
Blockly.Msg["self"]="当前状态机";
Blockly.Msg["Target"]="目标";
Blockly.Msg["Position"]="坐标";
Blockly.Msg["PositionX"]="X轴坐标";
Blockly.Msg["PositionY"]="Y轴坐标";
Blockly.Msg["PositionZ"]="Z轴坐标";
Blockly.Msg["Rotation"]="旋转";
Blockly.Msg["RotationX"]="X轴旋转";
Blockly.Msg["RotationY"]="Y轴旋转";
Blockly.Msg["RotationZ"]="Z轴旋转";
//**事件
//*基础
Blockly.Msg["Start"] = "开始";
Blockly.Msg["Update"] = "更新";
Blockly.Msg["FixedUpdate"] = "固定更新";
Blockly.Msg["LateUpdate"] = "最后更新";
Blockly.Msg["OnDestroy"] = "销毁时";
Blockly.Msg["Awake"] = "对象初始化";
Blockly.Msg["StateEnter"] = "状态初始化";
Blockly.Msg["OnEnable"] = "激活对象";
Blockly.Msg["OnDisable"] = "禁用对象";

//*碰撞
Blockly.Msg["OnCollisionEnter"] = "碰撞进入";
Blockly.Msg["OnCollisionExit"] = "碰撞离开";
Blockly.Msg["OnCollisionStay"] = "碰撞停留";
//*触发器
Blockly.Msg["OnTriggerEnter"] = "碰撞触发器进入";
Blockly.Msg["OnTriggerExit"] = "碰撞触发器离开";
Blockly.Msg["OnTriggerStay"] = "碰撞触发器停留";
//*碰撞(2D)
Blockly.Msg["OnCollisionEnter2D"] = "碰撞进入(2D)";
Blockly.Msg["OnCollisionExit2D"] = "碰撞离开(2D)";
Blockly.Msg["OnCollisionStay2D"] = "碰撞停留(2D)";
//*触发器(2D)
Blockly.Msg["OnTriggerEnter2D"] = "碰撞触发器进入(2D)";
Blockly.Msg["OnTriggerExit2D"] = "碰撞触发器离开(2D)";
Blockly.Msg["OnTriggerStay2D"] = "碰撞触发器停留(2D)";
//*系统
Blockly.Msg["OnApplicationFocus"] = "应用焦点变化";
Blockly.Msg["OnApplicationPause"] = "应用暂停";
Blockly.Msg["OnApplicationExit"] = "应用退出";
//*数据
Blockly.Msg["AddData"] = "用户库增加一条数据";
Blockly.Msg["AddDatas"] = "用户库增加多条数据";
Blockly.Msg["DelData"] = "用户库删除一条数据";
Blockly.Msg["DelDatas"] = "用户库删除多条数据";
Blockly.Msg["UpData"] = "用户库修改一条数据";
Blockly.Msg["UpDatas"] = "用户库修改多条数据";
//*支付
Blockly.Msg["PayOrderCreated"] = "订单创建完成";
Blockly.Msg["PaySuccess"] = "支付成功";
Blockly.Msg["PayFail"] = "支付失败(暂时没实现)";
//*鼠标
//与带碰撞体的对象交互
Blockly.Msg["OnMouseDown"] = "鼠标按下";
Blockly.Msg["OnMouseUp"] = "鼠标抬起";
Blockly.Msg["OnMouseDrag"] = "鼠标拖拽";
Blockly.Msg["OnMouseEnter"] = "鼠标移入";
Blockly.Msg["OnMouseExit"] = "鼠标移出";
Blockly.Msg["OnMouseOver"] = "鼠标悬停";
Blockly.Msg["OnMouseUpAsButton"] = "鼠标抬起(按下对象)";
//与UGUI交互
Blockly.Msg["OnPointerDown"] = "鼠标按下(UI)";
Blockly.Msg["OnPointerUp"] = "鼠标抬起(UI)";
Blockly.Msg["OnPointerEnter"] = "鼠标移入(UI)";
Blockly.Msg["OnPointerExit"] = "鼠标移出(UI)";
Blockly.Msg["OnPointerClick"] = "鼠标点击(UI)";
Blockly.Msg["OnInitializePotentialDrag"] = "鼠标可能发生拖拽(UI)";
Blockly.Msg["OnBeginDrag"] = "鼠标开始拖拽(UI)";
Blockly.Msg["OnDrag"] = "鼠标拖拽中(UI)";
Blockly.Msg["OnEndDrag"] = "鼠标结束拖拽(UI)";
Blockly.Msg["OnDrop"] = "鼠标结束拖拽(非拖拽中对象)(UI)";
Blockly.Msg["OnScroll"] = "鼠标滚轮滚动(UI)";
Blockly.Msg["OnUpdateSelected"] = "选中对象(UI)";
Blockly.Msg["OnSelect"] = "开始选中对象(UI)";
Blockly.Msg["OnDeselect"] = "取消选中对象(UI)";
Blockly.Msg["OnMove"] = "移动时(UI)";
//用户
Blockly.Msg["KickUserOff"] = "用户被踢下线";




/*jshint sub:true*/

Blockly.Msg["StartPoint"] = "起点";
Blockly.Msg["CenterPoint"] = "中心";
Blockly.Msg["unityengine_Vertical"] = "垂直";
Blockly.Msg["unityengine_Horizontal"] = "水平";
Blockly.Msg["TEXT_CHARAT_TAIL"] = "";
Blockly.Msg["TEXT_GET_SUBSTRING_TAIL"] = "";
Blockly.Msg["LISTS_GET_INDEX_TAIL"] = "";
Blockly.Msg["LISTS_GET_SUBLIST_TAIL"] = "";
Blockly.Msg["ORDINAL_NUMBER_SUFFIX"] = "";
Blockly.Msg["PROCEDURES_DEFNORETURN_DO"] = "";
Blockly.Msg["OpenBlock.UResources.Resources_Load"] = "加载资源 %1";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_root"] = "%1层级面板中最外层对象";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_activeInHierarchy"] = "%1在层级面板中的激活状态";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_isStatic"] = "%1的静态状态";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_active"] = "设置 %1 活跃状态为 %2";
Blockly.Msg["OpenBlock.URenderer.Set_gameobject_renderer_enable"] = "设置 %1 全部子对象渲染组件(Renderer)状态为 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_destroy"] = "销毁 %1";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_3"] = "创建预制体对象 %1 位置 %2 欧拉角 %3 父节点 %4";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_1"] = "创建预制体对象 %1 位置 %2 欧拉角 %3";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_parent"] = "设置 %1 父节点为 %2";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_tag"] = "设置 %1 标签为 %2";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_dontDestroyOnSceneLoad"] = "设置 %1 切换场景时不被销毁";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_destroyByTime"] = "%1 秒后销毁 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_destroyAllChild"] = "销毁 %1 全部子对象";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_layerByName"] = "设置 %1 层为 %2";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_index"] = "设置 %1 层级面板位置序号为 %2";
Blockly.Msg["OpenBlock.UGameObject.Add_openblockbehaver"] = "在 %1 上添加openblock脚本 %2";
Blockly.Msg["OpenBlock.UGameObject.Add_component"] = "在 %1 上添加 %2 组件";
Blockly.Msg["OpenBlock.UGameObject.Find_gameobject_with_id"] = "查找场景中ID为 %1 的游戏对象";
Blockly.Msg["OpenBlock.UGameObject.Find_gameobject_child_by_index"] = "查找 %1 的第 %2 个子对象";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_parent"] = "%1 的父节点";
Blockly.Msg["OpenBlock.UGameObject.Null_gameobject"] = "空游戏对象";
Blockly.Msg["OpenBlock.UGameObject.Find_gameobject_with_name"] = "查找名称为 %1 的游戏对象";
Blockly.Msg["OpenBlock.UGameObject.Find_gameobject_with_name_in_child"] = "查找 %1 子节点中名称为 %2 的游戏对象";
Blockly.Msg["OpenBlock.UGameObject.Find_gameobject_with_tag"] = "查找场景中标签为 %1 的游戏对象";
Blockly.Msg["OpenBlock.UGameObject.Find_gameobjects_with_tag"] = "查找场景中标签为 %1 的全部游戏对象";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_noreturn_1"] = "创建预制体对象 %1 位置 %2 欧拉角 %3";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_noreturn_3"] = "创建预制体对象 %1 位置 %2 欧拉角 %3 父节点 %4";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_noreturn_2"] = "创建预制体对象 %1 父节点 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_2"] = "创建预制体对象 %1 父节点 %2";
Blockly.Msg["OpenBlock.UGameObject.Get_transform"] = "%1 的三维变换";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_position"] = "%1 的%2三维坐标";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_rotation"] = "%1 的%2四元数";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_euler"] = "%1 的%2欧拉角";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_position_single"] = "%1 的 %2 三维坐标的 %3";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_euler_single"] = "%1 的%2欧拉角的 %3";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_inital_size_single"] = "%1 的碰撞器大小 %2";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_collider_center_single"] = "%1 的碰撞器中心 %2";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_capsule_collider_size_single"] = "%1 胶囊碰撞器 的%2";
Blockly.Msg["unityengine_get_gameobject_inital_size_single_opt_width"] = "x";
Blockly.Msg["unityengine_get_gameobject_inital_size_single_opt_height"] = "y";
Blockly.Msg["unityengine_get_gameobject_inital_size_single_opt_depth"] = "z";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_scale_single"] = "%1 本地缩放比例的 %2";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_scale"] = "%1 的本地缩放比例";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_tag"] = "%1 的标签";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_layer"] = "%1 的层";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_axis_single"] = "%1 的%2的方向向量";
Blockly.Msg["unityengine_get_gameobject_axis_single_opt_x"] = "x(红)轴";
Blockly.Msg["unityengine_get_gameobject_axis_single_opt_y"] = "y(绿)轴";
Blockly.Msg["unityengine_get_gameobject_axis_single_opt_z"] = "z(蓝)轴";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_name"] = "%1 的名称";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_active_state"] = "%1 的激活状态";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_child_count"] = "%1 的子对象数量";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_id"] = "%1 的ID";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_index"] = "%1 的层级面板序号";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_TransformDirection"] = "%1 的 %2 方向向量";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_TransformPoint"] = "%1 偏移 %2 的位置";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject2D_TransformDirection"] = "%1 的 %2 方向向量";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_TransformDirectionPosition"] = "%1 的 %2 方向 %3距离的坐标";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject2D_TransformDirectionPosition"] = "%1 的 %2 方向 %3距离的坐标";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject2D_TransformPoint"] = "%1 偏移 %2 的位置";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_position"] = "设置 %1 的%3坐标为 %2";
Blockly.Msg["unityengine_world"] = "世界";
Blockly.Msg["unityengine_local"] = "本地";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_move_towards"] = "%1 以每秒 %2 的速度向 %3 位置移动(%4)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_transform_translate"] = "%1 以每秒 %2 的速度沿着%3 方向移动(%4)";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_3d_position_single"] = "设置 %1 的 %4坐标的%3 为 %2";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_around_gameobject_by_axis"] = "设置 %1 位置为以%2为中心，距离%3偏移角度%4轴%5，基于%6";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_around_gameobject_2D"] = "移动%1到以中心%2距离%3角度%4的位置上";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_eluer"] = "设置 %1 %3 欧拉角为 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_lookat_positon"] = "%1 转向 位置%2 (世界)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_turnto_positon"] = "%1 以每秒 %2 的速度转向位置%3 (世界)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_axis_lookat_positon"] = "%1的轴%3转向 位置%2 (世界)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_axis_turnto_positon"] = "%1的轴%4以每秒 %2 的速度转向位置%3 (世界)";
Blockly.Msg["OpenBlock.UGameObject.GameObjectTurnToEluer"] = "%1 以每秒 %3 的速度转到欧拉角%2(世界)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotate_euler"] = "%1 每秒旋转 %2 欧拉角(%3)";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_quaternion"] = "设置 %1 的%3四元数 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_ratate_around_position"] = "%1 围绕 %2 坐标(世界)以每秒 %3 角度沿 %4 轴旋转";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_scale"] = "设置 %1 缩放比例为 %2";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_scale_single"] = "设置 %1 的 %3 缩放比例为 %2";
Blockly.Msg["OpenBlock.UPhysics.Gameobject_add_collider_component"] = "向 %1 添加3D %2 组件";
Blockly.Msg["unityengine_boxcollider"] = "盒子碰撞器(Box\u00A0Collider)";
Blockly.Msg["unityengine_spherecollider"] = "球体碰撞器(Sphere\u00A0Collider)";
Blockly.Msg["unityengine_capsulecollider"] = "胶囊碰撞器(Capsule\u00A0Collider)";
Blockly.Msg["unityengine_wheelcollider"] = "轮子碰撞器(Wheel\u00A0Collider)";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_sphere_collider_radius"] = "%1 的球体碰撞器(Sphere Collider)半径";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_box_collider_size_single"] = "%1 的盒子碰撞器(Box Collider)大小(不受缩放影响)";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_rotate_and_wait"] = "%1 在 %2 秒内旋转到 %3 欧拉角(世界)[完成后继续]";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_cannot_out_screen_2d"] = "限制 %1 无法超出屏幕(2D)";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider_center_single"] = "设置 %1的%3 的 %4 中心为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider_center"] = "设置 %1的%3 中心为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider2D_center_single"] = "设置 %1的%3 位置偏移为 %4 为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider2D_center"] = "设置 %1的%3 位置偏移为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_boxcollider_size"] = "设置 %1 盒子碰撞器(Box Collider)大小为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_boxcollider_size_single"] = "设置 %1 盒子碰撞器(Box Collider)大小的 %3 为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_boxcollider2D_size_single"] = "设置 %1 矩形碰撞器(Box Collider2D)大小的 %3 为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider_enable"] = "设置 %1 碰撞器组件激活状态为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider_trigger_enable"] = "设置 %1 碰撞器组件触发器状态为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_sphere_collider_radius"] = "设置 %1 球体碰撞器(Sphere Collider)半径为 %2";
Blockly.Msg["unityengine_radius"] = "半径";
Blockly.Msg["unityengine_height"] = "高";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_capsule_collider_size_single"] = "设置 %1 胶囊碰撞器(capsule Collider)%3 为 %2";
Blockly.Msg["OpenBlock.UPhysics.Gameobject_add_collider2d_component"] = "向 %1 添加2D %2 组件";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_wheel_collider_base_attr"] = "%1 的轮子碰撞器的%2";
Blockly.Msg["unityengine_mass"] = "质量";
Blockly.Msg["unityengine_wheelDampingRate"] = "阻尼率";
Blockly.Msg["unityengine_suspensionDistance"] = "悬挂距离";
Blockly.Msg["unityengine_forceAppPointDistance"] = "悬挂作用点与轮胎底部距离";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_wheel_collider_base_attr"] = "设置 %1 的轮子碰撞器的%3为%2";
Blockly.Msg["unityengine_box_collider2d"] = "矩形碰撞器(BoxCollider2D)";
Blockly.Msg["unityengine_circle_collider2d"] = "圆形碰撞器(CircleCollider2D)";
Blockly.Msg["unityengine_capsule_collider2d"] = "胶囊碰撞器(CapsuleCollider2D)";
Blockly.Msg["unityengine_polygon_collider2d"] = "多边形碰撞器(PolygonCollider2D)";
Blockly.Msg["unityengine_edge_collider2d"] = "边缘碰撞器(EdgeCollider2D)";
Blockly.Msg["unityengine_composite_collider2d"] = "合成碰撞器(CompositeCollider2D)";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_capsule2d_collider_size"] = "%1 的2D胶囊碰撞器的大小";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_capsule2d_collider_size"] = "设置 %1 的2D胶囊碰撞器的大小为%2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_capsule2d_collider_direction"] = "设置 %1 的胶囊碰撞器的方向为%2";
Blockly.Msg["unityengine_direction_Vertical"] = "竖";
Blockly.Msg["unityengine_direction_Horizontal"] = "横";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider2d_offset"] = "设置 %1 的%3 碰撞器(2D)的位置偏移为%2";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_euler_z"] = "%1 的%2角度(2D)";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_position_2d"] = "%1 的%2二维坐标";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_position_2d_single"] = "%1 的%3二维坐标的 %2";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_scale_2d"] = "%1 本地缩放比例(2D)";
Blockly.Msg["OpenBlock.UGameObject.Get_gameobject_scale_2d_single"] = "%1 本地缩放比例的 %2";
Blockly.Msg["unityengine_get_world_pos_edge_in_maincamera"] = "主摄像机中 %1 屏幕边缘的世界坐标(2D)";
Blockly.Msg["unityengine_get_world_pos_edge_in_maincamera_opt_top"] = "上";
Blockly.Msg["unityengine_get_world_pos_edge_in_maincamera_opt_down"] = "下";
Blockly.Msg["unityengine_get_world_pos_edge_in_maincamera_opt_left"] = "左";
Blockly.Msg["unityengine_get_world_pos_edge_in_maincamera_opt_right"] = "右";
Blockly.Msg["OpenBlock.UGameObject.Get_direction_euler_2d"] = "%1 方向的角度(2D)";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_top"] = "上";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_down"] = "下";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_left"] = "左";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_right"] = "右";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_topleft"] = "左上";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_topright"] = "右上";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_downleft"] = "左下";
Blockly.Msg["unityengine_get_direction_euler_2d_opt_downright"] = "右下";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_position_2d"] = "设置 %1 的%3坐标为 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_move_towards_2d"] = "%1 以每秒 %2 的速度向 %3 位置移动";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_move_lerp_2d"] = "%1 以每秒 %2 的速度向 %3 位置移动(线性插值)";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_position_2d_single"] = "设置 %1 的%4坐标的%3为 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_move_direction_2d"] = "%1 以每秒 %2 的速度向 %3 移动";
Blockly.Msg["unityengine_gameobject_move_direction_2d_opt_up"] = "上";
Blockly.Msg["unityengine_gameobject_move_direction_2d_opt_down"] = "下";
Blockly.Msg["unityengine_gameobject_move_direction_2d_opt_left"] = "左";
Blockly.Msg["unityengine_gameobject_move_direction_2d_opt_right"] = "右";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_move_forward_back_2d"] = "%1 向 %3 移动 %2";
Blockly.Msg["unityengine_gameobject_move_forward_back_2d_opt_forward"] = "前";
Blockly.Msg["unityengine_gameobject_move_forward_back_2d_opt_back"] = "后";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_move_forward_back_2d_inspeed"] = "%1 根据自身角度以每秒 %2 的速度向 %3 移动";
Blockly.Msg["unityengine_gameobject_move_forward_back_2d_inspeed_opt_forward"] = "前";
Blockly.Msg["unityengine_gameobject_move_forward_back_2d_inspeed_opt_back"] = "后";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_eluer_2d"] = "%1 旋转到 %2 度(%3)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete2D"] = "%1 向%3旋转 %2 度(%4)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete2D_inspeed"] = "%1 每秒向%3旋转 %2 度(%4)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete_left"] = "%1 向左旋转 %2 度(%3)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete_right"] = "%1 向右旋转 %2 度(%3)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete_left_inspeed"] = "%1 每秒向左旋转 %2 度(%3)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete_right_inspeed"] = "%1 每秒向右旋转 %2 度(%3)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete_to_eluer_inspeed"] = "%1 以每秒 %2 的速度旋转到 %3 度(世界)";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_rotete_to_gameobject_inspeed"] = "%1 以每秒 %2 的速度旋转朝向 %3";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_turn_to_mouse"] = "%1 转向鼠标位置";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_turn_to_position_2d"] = "%1 转向 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_turn_to_gameobject_eluer"] = "%1 朝向 %2 时的角度";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_scale_2d"] = "设置 %1 缩放比例为 %2";
Blockly.Msg["OpenBlock.UGameObject.Set_gameobject_scale_2d_single"] = "设置%1 缩放比例的 %3 为 %2";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_collider_2d_size_single"] = "%1 的碰撞器(2D)大小 %2";
Blockly.Msg["unityengine_get_gameobject_collider_2d_size_single_opt_width"] = "宽";
Blockly.Msg["unityengine_get_gameobject_collider_2d_size_single_opt_height"] = "高";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_box_collider_2d_size"] = "设置 %1 的矩形碰撞器(Box Collider 2D)大小为 %2";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_box_collider_2d_size"] = "%1 矩形碰撞器(Box Collider 2D)的大小";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider2d_enable"] = "设置 %1 碰撞器(2D)组件激活状态为 %2";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_collider2d_trigger_enable"] = "设置 %1 碰撞器(2D)组件触发器状态为 %2";
Blockly.Msg["OpenBlock.UPhysics.Get_gameobject_circle_collider_radius"] = "%1 圆形碰撞器(Circle Collider 2D)的半径";
Blockly.Msg["OpenBlock.UPhysics.Set_gameobject_circle_collider_radius"] = "设置 %1 圆形碰撞器(Circle Collider 2D)的半径为 %2";
Blockly.Msg["OpenBlock.USpriteRenderer.Get_gameobject_spriterenderer_size_single"] = "%1 精灵组件(Sprite Renderer)的大小 %2";
Blockly.Msg["unityengine_get_gameobject_spriterenderer_size_single_opt_width"] = "x";
Blockly.Msg["unityengine_get_gameobject_spriterenderer_size_single_opt_height"] = "y";
Blockly.Msg["OpenBlock.USpriteRenderer.Gameobject_spriterenderer_is_outofscreen"] = "%1 精灵组件(Sprite Renderer)图片是否超出屏幕边缘";
Blockly.Msg["OpenBlock.USpriteRenderer.Get_gameobject_spriterenderer_slip"] = "%1 精灵组件(Sprite Renderer)%2的激活状态";
Blockly.Msg["unityengine_slipX"] = "水平翻转";
Blockly.Msg["unityengine_slipY"] = "垂直翻转";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_image_fromres"] = "设置 %1 精灵组件(Sprite Renderer)图片为资源目录 %2 的图片";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_material_color_rgba"] = "设置 %1 精灵组件(Sprite Renderer)材质颜色为 %2";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_color_rgba"] = "设置 %1 精灵组件(Sprite Renderer)颜色为 %2";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_color_alpha"] = "设置 %1 精灵组件(Sprite Renderer)不透明度为 %2(0-1)";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_order"] = "设置 %1 精灵组件(Sprite Renderer)图层顺序为 %2";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_sortingLayerName"] = "设置 %1 精灵组件(Sprite Renderer)排序层为 %2(可能改为下拉菜单)";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_sortingLayerId"] = "设置 %1 精灵组件(Sprite Renderer)排序层为 %2";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_slip"] = "设置 %1 精灵组件(Sprite Renderer)%3激活状态为 %2";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_image"] = "设置 %1 精灵组件(Sprite Renderer)图片为 %2";
Blockly.Msg["OpenBlock.USpriteRenderer.Set_gameobject_spriterenderer_MainMaterialOffset"] = "设置 %1 精灵组件(Sprite Renderer)主材质位置偏移量为 %2";
Blockly.Msg["OpenBlock.UParticle.Gameobject_particles_system_play"] = "%1 粒子组件(Particles System)开始播放";
Blockly.Msg["OpenBlock.UParticle.Gameobject_particles_system_stop"] = "%1 粒子组件(Particles System)停止播放";
Blockly.Msg["OpenBlock.UParticle.Gameobject_particles_system_stop_state"] = "%1 粒子组件(Particles System)是否停止播放";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animation_play"] = "%1 动画组件(Animation)播放名称为 %2 的动画";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animator_set_bool"] = "设置 %1 动画状态机组件(Animator)名称为 %2 的布尔值为 %3";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animator_set_float"] = "设置 %1 动画状态机组件(Animator)名称为 %2 的浮点值为 %3";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animator_set_play_speed"] = "设置 %1 动画状态机组件(Animator)播放速率为 %2";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animator_set_trigger"] = "激活 %1 动画状态机组件(Animator)名称为 %2 的触发器";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animation_set_all_play_speed"] = "设置 %1 动画组件(Animation)全部动作播放速率为 %2";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animation_set_play_time"] = "设置 %1 动画组件(Animation)名称为 %2 的动作播放时间进度为 %3";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animation_stop"] = "%1 动画组件(Animation)停止播放动画";
Blockly.Msg["OpenBlock.UAnimator.Gameobject_animator_get_play_name"] = "%1 动画状态机组件(Animator)层序号%2层名称%3是否在播放 %4 动画";
Blockly.Msg["OpenBlock.UAnimator.Get_gameobject_animator_is_finish"] = "%1 动画状态机组件(Animator)当前动画是否播放完成";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_shake_position_and_wait"] = "%1 在 %2 方向偏移 %3 角度 %4 震动 %5 次[完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_shake_position"] = "%1 在 %2 方向偏移 %3 角度 %4 震动 %5 次";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_camera_shake_position_and_wait"] = "%1 摄像机组件(Camera)在 %2 方向偏移 %3 角度 %4 震动 %5 次[完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_camera_shake_position"] = "%1 摄像机组件(Camera)在 %2 方向偏移 %3 角度 %4 震动 %5 次";
Blockly.Msg["OpenBlock.ULineRenderer.Set_gameobject_linerenderer_material_maintexture_scale"] = "设置 %1 线渲染器组件(Line Renderer)默认贴图大小比例为 %2";
Blockly.Msg["OpenBlock.ULineRenderer.Gameobject_linerenderer_set_point_position"] = "设置 %1 线渲染器组件(Line Renderer)第 %2 个顶点的位置为 %3";
Blockly.Msg["OpenBlock.ULineRenderer.Gameobject_linerenderer_set_point_count"] = "设置 %1 线渲染器(Line Renderer)顶点数量为 %2";
Blockly.Msg["OpenBlock.UMath.Unityengine_vector3"] = "x %1 y %2 z %3";
Blockly.Msg["OpenBlock.UControl.Wait_secend"] = "等待 %1 秒";
Blockly.Msg["OpenBlock.UMath.Unityengine_vector2"] = "x %1 y %2";
Blockly.Msg["OpenBlock.UMath.Vector3_magnitude"] = "%1的长度";
Blockly.Msg["OpenBlock.UMath.Vector2_magnitude"] = "%1的长度";
Blockly.Msg["OpenBlock.UMath.Vector3_sqrMagnitude"] = "%1的长度平方";
Blockly.Msg["OpenBlock.UMath.Vector2_sqrMagnitude"] = "%1的长度平方";
Blockly.Msg["OpenBlock.UMath.Math_vector2_changeMagnitude"] = "%1长度改为%2";
Blockly.Msg["OpenBlock.UMath.Math_vector3_changeMagnitude"] = "%1长度改为%2";
Blockly.Msg["OpenBlock.UMath.Math_vector2_distance_clamp"] = "限制%1离%2的距离在[%3,%4]范围内";
Blockly.Msg["OpenBlock.UMath.Unary_operation"] = "%1 的 %2";
Blockly.Msg["OpenBlock.UMath.mathf_infinity"] = "正无穷";
Blockly.Msg["OpenBlock.UMath.mathf_negativeInfinity"] = "负无穷";
Blockly.Msg["unityengine_math_unary_operation_opt_Abs"] = "绝对值";
Blockly.Msg["unityengine_math_unary_operation_opt_Opposite"] = "相反数";
Blockly.Msg["unityengine_math_unary_operation_opt_Sqrt"] = "平方根";
Blockly.Msg["unityengine_math_unary_operation_opt_Sin"] = "正弦";
Blockly.Msg["unityengine_math_unary_operation_opt_Asin"] = "反正弦";
Blockly.Msg["unityengine_math_unary_operation_opt_Cos"] = "余弦";
Blockly.Msg["unityengine_math_unary_operation_opt_Acos"] = "反余弦";
Blockly.Msg["unityengine_math_unary_operation_opt_Tan"] = "正切";
Blockly.Msg["unityengine_math_unary_operation_opt_Cot"] = "余切";
Blockly.Msg["unityengine_math_unary_operation_opt_Round"] = "四舍五入";
Blockly.Msg["unityengine_math_unary_operation_opt_Ceiling"] = "向上取整";
Blockly.Msg["unityengine_math_unary_operation_opt_Floor"] = "向下取整";
Blockly.Msg["OpenBlock.UMath.Default_value"] = "默认值";
Blockly.Msg["OpenBlock.UMath.Mathematical_constant"] = "%1";
Blockly.Msg["unityengine_math_mathematical_constant_opt_PI"] = "π";
Blockly.Msg["unityengine_math_mathematical_constant_opt_Rad2Deg"] = "弧度->度 转换常数";
Blockly.Msg["unityengine_math_mathematical_constant_opt_Deg2Rad"] = "度->弧度 转换常数";
Blockly.Msg["OpenBlock.UMath.Quaternion_identity"] = "四元数(无旋转)";
Blockly.Msg["OpenBlock.UMath.Euler_to_quatenion"] = "%1 转为四元数";
Blockly.Msg["OpenBlock.UMath.Math_Quatenion_Slerp"] = "[%1,%2]之间，插值%3的值";
Blockly.Msg["OpenBlock.UMath.Math_quatenion"] = "四元数 x %1 y %2 z %3 w %4";
Blockly.Msg["OpenBlock.UMath.Quatenion_to_euler_single"] = "%1 对应欧拉角的 %2";
Blockly.Msg["OpenBlock.UMath.Number_to_string"] = "%1 转为字符串";
Blockly.Msg["OpenBlock.UMath.Get_direction_quatenion"] = "朝向 %1 的四元数";
Blockly.Msg["OpenBlock.UMath.String_to_number"] = "%1 转为数值";
Blockly.Msg["OpenBlock.UMath.Random_int_include"] = "随机整数[ %1 , %2 ]";
Blockly.Msg["OpenBlock.UMath.Random_int_exclusive"] = "随机整数[ %1 , %2 )";
Blockly.Msg["OpenBlock.UMath.Random_float_include"] = "随机数[ %1 , %2 ]";
Blockly.Msg["OpenBlock.UMath.World_to_screen_position"] = "世界坐标 %1 的屏幕坐标";
Blockly.Msg["OpenBlock.UMath.Screen_to_world_position"] = "屏幕坐标 %1 的世界坐标";
Blockly.Msg["OpenBlock.UMath.World_to_canvas_position"] = "世界坐标 %1 位于画布 %2 上的坐标";
Blockly.Msg["OpenBlock.UMath.Screen_to_canvas_position"] = "屏幕坐标 %1 位于画布 %2 上的坐标";
Blockly.Msg["OpenBlock.UMath.Rotate_vector3"] = "将 %1 旋转 %2";
Blockly.Msg["OpenBlock.UMath.Quatenion_to_euler"] = "四元数 %1 的欧拉角";
Blockly.Msg["OpenBlock.UMath.Vector3_direction"] = "三维向量 %1";
Blockly.Msg["OpenBlock.UMath.Vector2_direction"] = "二维向量 %1";
Blockly.Msg["unityengine_math_vector3_direction_opt_forword"] = "前";
Blockly.Msg["unityengine_math_vector3_direction_opt_back"] = "后";
Blockly.Msg["unityengine_math_vector3_direction_opt_left"] = "左";
Blockly.Msg["unityengine_math_vector3_direction_opt_right"] = "右";
Blockly.Msg["unityengine_math_vector3_direction_opt_up"] = "上";
Blockly.Msg["unityengine_math_vector3_direction_opt_down"] = "下";
Blockly.Msg["unityengine_math_vector_direction_opt_zero"] = "零";
Blockly.Msg["unityengine_math_vector_direction_opt_one"] = "一";
Blockly.Msg["OpenBlock.UMath.Math_clamp"] = "限制 %1 在[ %2 , %3 ]范围内";
Blockly.Msg["OpenBlock.UMath.Math_number_lerp"] = "[ %1 , %2 ]之间，插值位置 %3 的值";
Blockly.Msg["OpenBlock.UMath.Math_number_max"] = "%1 与 %2 较大的值";
Blockly.Msg["OpenBlock.UMath.Math_number_min"] = "%1 与 %2 较小的值";
Blockly.Msg["OpenBlock.UMath.Mathf_MoveTowards"] = "移动值从%1到%2，最大变化%3";
Blockly.Msg["OpenBlock.UMath.Mathf_MoveTowardsAngle"] = "移动角度从%1到%2，最大变化%3";
Blockly.Msg["OpenBlock.UMath.Mathf_PingPong"] = "乒乓%1值，最大为%2";
Blockly.Msg["OpenBlock.UMath.Mathf_Repeat"] = "循环%1值，最大为%2";
Blockly.Msg["OpenBlock.UMath.Math_vector2_addition"] = "%1 + %2";
Blockly.Msg["OpenBlock.UMath.Math_vector2_subtraction"] = "%1 - %2";
Blockly.Msg["OpenBlock.UMath.Math_vector2_multiplies"] = "%1 * %2";
Blockly.Msg["OpenBlock.UMath.Math_vector2_distance"] = "%1 与 %2 距离";
Blockly.Msg["OpenBlock.UMath.Math_vector2_getvalue"] = "%1 %2";
Blockly.Msg["OpenBlock.UMath.Math_vector2_magnitude"] = "%1 的长度";
Blockly.Msg["OpenBlock.UMath.Math_vector2_normalized"] = "%1 单位向量";
Blockly.Msg["OpenBlock.UMath.Math_vector2_dot"] = "%1 点乘 %2";
Blockly.Msg["OpenBlock.UMath.Math_vector2_direction_position"] = "以%1角度向前移动%2距离后的位置偏移值";
Blockly.Msg["OpenBlock.UMath.Math_Vector2_Lerp"] = "[ %1 , %2 ]之间，插值位置 %3 的值";
Blockly.Msg["OpenBlock.UMath.Math_Vector3_Lerp"] = "[ %1 , %2 ]之间，插值位置 %3 的值";
Blockly.Msg["OpenBlock.UMath.Math_vector3_addition"] = "%1 + %2";
Blockly.Msg["OpenBlock.UMath.Math_vector3_subtraction"] = "%1 - %2";
Blockly.Msg["OpenBlock.UMath.Math_vector3_multiplies"] = "%1 * %2";
Blockly.Msg["OpenBlock.UMath.Math_vector3_getvalue"] = "%1 %2";
Blockly.Msg["OpenBlock.UMath.Math_vector3_normalized"] = "%1 单位向量";
Blockly.Msg["OpenBlock.UMath.Math_vector3_distance"] = "%1 与 %2 距离";
Blockly.Msg["OpenBlock.UMath.Math_vector3_center_point"] = "%1 与 %2 中心";
Blockly.Msg["OpenBlock.UMath.Gameobject_vector3_included_angle"] = "%1 与 %2 的夹角";
Blockly.Msg["OpenBlock.UMath.positiona_lookat_positionb_quatenion"] = "%1 朝向 %2 的四元数";
Blockly.Msg["OpenBlock.UMath.Math_quaternion_multiplies"] = "%1 * %2";
Blockly.Msg["OpenBlock.UMath.Math_vector3_reflect"] = "入射向量%1法向量%2的反射向量";
Blockly.Msg["OpenBlock.UMath.Math_vector2_reflect"] = "入射向量%1法向量%2的反射向量";
Blockly.Msg["OpenBlock.UMath.Math_quaternion_FromToRotation"] = "%1向量朝向%2向量的四元数";
Blockly.Msg["OpenBlock.UMath.Math_judge_equal"] = "%1 是 %2";
Blockly.Msg["OpenBlock.UMath.Math_logic_mathematical"] = "%1 %2 %3";
Blockly.Msg["unityengine_math_logic_mathematical_opt_or"] = "或";
Blockly.Msg["unityengine_math_logic_mathematical_opt_and"] = "且";
Blockly.Msg["OpenBlock.UMath.Math_input_is_null"] = "%1 为无效值";
Blockly.Msg["OpenBlock.UMath.Math_input_is_not_null"] = "%1 不为无效值";
Blockly.Msg["OpenBlock.UTime.Set_time_scale"] = "设置时间流速为 %1";
Blockly.Msg["OpenBlock.UTime.Time_deltatime"] = "上一帧耗费时间";
Blockly.Msg["OpenBlock.UTime.Time_frame_count"] = "已运行帧数";
Blockly.Msg["OpenBlock.UTime.Time_scene_run_seconds"] = "当前场景运行时间(秒)";
Blockly.Msg["OpenBlock.UTime.Time_run_seconds"] = "总运行时间(秒)";
Blockly.Msg["OpenBlock.UTouch.Input_get_touch"] = "第 %1 个触摸点信息";
Blockly.Msg["OpenBlock.UTouch.Input_touch_count"] = "触摸点数量";
Blockly.Msg["OpenBlock.UTouch.Input_get_touchs"] = "全部触摸点信息";
Blockly.Msg["OpenBlock.UTouch.Touch_delta_position"] = "%1 的增量坐标";
Blockly.Msg["OpenBlock.UTouch.Touch_finger_id"] = "%1 的手指ID";
Blockly.Msg["OpenBlock.UTouch.Touch_state"] = "%1 处于%2状态";
Blockly.Msg["OpenBlock.UTouch.Touch_position"] = "%1 坐标";
Blockly.Msg["OpenBlock.UTouch.Input_get_touchIndex_by_fingerID"] = "手指ID%1的触摸点序号";

Blockly.Msg["TouchStateMenu"] = [
    ["开始", "Began"],
    ["移动", "Moved"],
    ["结束", "Ended"],
    ["静止", "Stationary"],
];
//UGUI
Blockly.Msg["OpenBlock.UCanvas.Set_anchoredPosition_single"] = "设置 %1 的矩形变换 %3 坐标为 %2";
Blockly.Msg["OpenBlock.UCanvas.Set_anchoredPosition"] = "设置 %1 矩形变换坐标为 %2";
Blockly.Msg["OpenBlock.UCanvas.Set_anchoredOffsetMin"] = "设置 %1 矩形变换最小位置偏移为 %2";
Blockly.Msg["OpenBlock.UCanvas.Set_anchoredOffsetMax"] = "设置 %1 矩形变换最大位置偏移为 %2";
Blockly.Msg["OpenBlock.UCanvas.Canvas_set_scale"] = "设置 %1 矩形变换缩放比例为 %2";
Blockly.Msg["OpenBlock.UCanvas.Canvas_set_size"] = "设置 %1 矩形变换相对于锚点的距离为 %2";
Blockly.Msg["OpenBlock.UCanvas.Canvas_set_size_single"] = "设置 %1 的矩形变换 %3 相对于锚点的距离为 %2";
Blockly.Msg["OpenBlock.UCanvas.Canvas_get_referencePixelsPerUnit"] = "%1 的画布每单位像素";
Blockly.Msg["unityengine_gameobject_canvas_set_size_single_opt_x"] = "宽";
Blockly.Msg["unityengine_gameobject_canvas_set_size_single_opt_y"] = "高";
Blockly.Msg["OpenBlock.UCanvas.Canvas_set_layer_top"] = "%1 顶置层级";
Blockly.Msg["OpenBlock.UDebug.Debug_log"] = "显示日志 %1";
Blockly.Msg["OpenBlock.UDebug.Debug_LogWarning"] = "显示警告%1";
Blockly.Msg["OpenBlock.UDebug.Debug_LogError"] = "显示错误%1";
Blockly.Msg["OpenBlock.UDebug.Debug_DrawLine"] = "画线起点%1终点%2颜色%3持续时间(秒)%4深度测试%5(调试)";
Blockly.Msg["OpenBlock.UDebug.Debug_DrawRay"] = "画射线起点%1终点%2颜色%3持续时间(秒)%4深度测试%5(调试)";
Blockly.Msg["OpenBlock.UCanvas.Canvas_rebuild_layout"] = "%1 更新矩形变换布局";
Blockly.Msg["OpenBlock.UCanvas.Canvas_ForceUpdateCanvases"] = "更新画布布局";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_from_res"] = "从资源目录 %1 创建游戏对象";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_from_res_1"] = "从资源目录 %1 创建游戏对象 父节点 %2";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_from_res_3"] = "从资源目录 %1 创建游戏对象 位置 %2 欧拉角 %3 父节点 %4";
Blockly.Msg["OpenBlock.UGameObject.Gameobject_instantiate_from_res_3_noreturn"] = "从资源目录 %1 创建游戏对象 位置 %2 欧拉角 %3 父节点 %4";
Blockly.Msg["OpenBlock.UCanvas.Canvas_get_offset_min"] = "%1 矩形变换最小位置偏移";
Blockly.Msg["OpenBlock.UCanvas.Canvas_get_offset_max"] = "%1 矩形变换最大位置偏移";
Blockly.Msg["OpenBlock.UCanvas.Canvas_get_position"] = "%1 矩形变换的坐标";
Blockly.Msg["OpenBlock.UCanvas.Canvas_get_position_single"] = "%1 矩形变换的 %2 坐标";
Blockly.Msg["OpenBlock.UCanvas.RectTransform_get_size_single"] = "%1 矩形变换 %2大小";
Blockly.Msg["OpenBlock.UCanvas.Canvas_get_sizeDelta_single"] = "%1 矩形变换的 %2相对于锚点的距离";
Blockly.Msg["unityengine_gameobject_canvas_get_size_single_opt_x"] = "x";
Blockly.Msg["unityengine_gameobject_canvas_get_size_single_opt_y"] = "y";
Blockly.Msg["OpenBlock.UCanvas.Canvas_get_layout_group_padding"] = "%1 布局组组件(Layout Group) %2 内边距";
Blockly.Msg["unityengine_gameobject_canvas_get_layout_group_padding_opt_left"] = "左";
Blockly.Msg["unityengine_gameobject_canvas_get_layout_group_padding_opt_right"] = "右";
Blockly.Msg["unityengine_gameobject_canvas_get_layout_group_padding_opt_top"] = "上";
Blockly.Msg["unityengine_gameobject_canvas_get_layout_group_padding_opt_bottom"] = "下";
Blockly.Msg["OpenBlock.UCanvas.Canvas_set_pivot"] = "设置 %1 轴点为 %2";
Blockly.Msg["OpenBlock.UCanvas.Canvas_set_layout_group_padding"] = "设置 %1 布局组组件(Layout Group) %3 内边距为 %2";
Blockly.Msg["unityengine_gameobject_canvas_set_layout_group_padding_opt_left"] = "左";
Blockly.Msg["unityengine_gameobject_canvas_set_layout_group_padding_opt_right"] = "右";
Blockly.Msg["unityengine_gameobject_canvas_set_layout_group_padding_opt_top"] = "上";
Blockly.Msg["unityengine_gameobject_canvas_set_layout_group_padding_opt_bottom"] = "下";
Blockly.Msg["OpenBlock.UCanvas.VerticalLayoutGroupGetSpacing"] = "%1 垂直布局组(VerticalLayoutGroup)间隔";
Blockly.Msg["OpenBlock.UCanvas.HorizontalLayoutGroupGetSpacing"] = "%1 水平布局组(HorizontalLayoutGroup)间隔";
Blockly.Msg["OpenBlock.UCanvas.Get_scrollbar_size"] = "%1 滚动条组件(Scrollbar)长度";
Blockly.Msg["OpenBlock.UCanvas.Get_scrollbar_value"] = "%1 滚动条组件(Scrollbar)进度";
Blockly.Msg["OpenBlock.UCanvas.Set_canvas_group_alpha"] = "设置 %1 画布组组件(Canvas Group)透明度为 %2 (0-1)";
Blockly.Msg["OpenBlock.UCanvas.Toggle_set_selection"] = "设置 %1 选则框组件(Toggle)选中状态为 %2";
Blockly.Msg["OpenBlock.UCanvas.Toggle_get_selection"] = "%1 选择框组件(Toggle)选中状态";
//Blockly.Msg["unityengine_dotween_gameobject_do_anchor_position_single_and_wait"] = "%1 在 %2 秒内将 %3 移动到 %4 [完成后继续]";
Blockly.Msg["OpenBlock.UCanvas.Image_set_enable"] = "设置 %1 图片组件(Image)激活状态为 %2";
Blockly.Msg["OpenBlock.UCanvas.Set_image_from_res"] = "设置 %1 图片组件(Image)源图为 %2";
Blockly.Msg["OpenBlock.UCanvas.Image_get_pivot"] = "%1 图片组件(Image)中心点";
Blockly.Msg["OpenBlock.UCanvas.Image_set_color"] = "%1 图片组件(Image)颜色改为 %2";
Blockly.Msg["OpenBlock.UCanvas.GraphicRaycaster_Get_RaycastCount"] = "%1图片射线组件(GraphicRaycaster)上位置%2碰到的游戏对象数量";


Blockly.Msg["OpenBlock.UMath.Unityengine_color_RGB"] = "色值 R %1 G %2 B %3";
Blockly.Msg["OpenBlock.UMath.Unityengine_color"] = "色值 R %1 G %2 B %3 A %4";
Blockly.Msg["OpenBlock.UMath.Unityengine_hsv_color"] = "色值 H %1 S %2 V %3";
Blockly.Msg["OpenBlock.UMath.Unityengine_normal_color"] = "%1";
Blockly.Msg["Color_black"] = "黑色";
Blockly.Msg["Color_blue"] = "蓝色";
Blockly.Msg["Color_clear"] = "无色透明";
Blockly.Msg["Color_cyan"] = "蓝绿";
Blockly.Msg["Color_gray"] = "灰色";
Blockly.Msg["Color_green"] = "绿色";
Blockly.Msg["Color_magenta"] = "洋红";
Blockly.Msg["Color_red"] = "红色";
Blockly.Msg["Color_white"] = "白色";
Blockly.Msg["Color_yellow"] = "黄色";
Blockly.Msg["OpenBlock.UMath.Unityengine_colorToHtmlStringRGB"] = "%1的字符串色值";
Blockly.Msg["OpenBlock.UMath.Unityengine_colorToHtmlStringRGBA"] = "%1的字符串色值";
Blockly.Msg["OpenBlock.UCanvas.Image_set_fill_amount"] = "%1 图片组件(Image)填充进度为 %2";
Blockly.Msg["OpenBlock.UCanvas.Image_set_material"] = "%1 图片组件(Image)材质为 %2";
Blockly.Msg["OpenBlock.UCanvas.Image_set_size_to_default"] = "%1 图片组件(Image)还原默认大小";
Blockly.Msg["OpenBlock.UCanvas.Set_image_opacity"] = "%1 图片组件(Image)透明值为 %2";
Blockly.Msg["OpenBlock.UCanvas.Image_set_image"] = "%1 图片组件(Image)源图为 %2";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_image_fade_and_wait"] = "%1 图片组件(Image)在 %2 秒内将透明值改为 %3 [完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_image_fade_and_message"] = "%1 图片组件(Image)在 %2 秒内将透明值改为 %3 完成后发送 %4 消息 附加信息 %5";
Blockly.Msg["OpenBlock.UCanvas.Text_set_enable"] = "设置 %1 文本组件(Text)激活状态为 %2";
Blockly.Msg["OpenBlock.UCanvas.Text_get_text"] = "%1 文本组件(Text)的文本内容";
Blockly.Msg["OpenBlock.UCanvas.Text_get_size_single"] = "%1 文本组件(Text)的文本内容 %2";
Blockly.Msg["unityengine_gameobject_text_get_size_single_opt_width"] = "宽";
Blockly.Msg["unityengine_gameobject_text_get_size_single_opt_height"] = "高";
Blockly.Msg["OpenBlock.UCanvas.Text_set_text"] = "设置 %1 文本组件(Text)文本内容为 %2";
Blockly.Msg["OpenBlock.UCanvas.Text_set_text_font"] = "设置 %1 文本组件(Text)文本字体为 %2";
Blockly.Msg["OpenBlock.UCanvas.Text_set_text_color"] = "设置 %1 文本组件(Text)颜色为 %2";
Blockly.Msg["OpenBlock.UCanvas.Text_set_text_font_size"] = "设置 %1 文本组件(Text)字号为 %2";
Blockly.Msg["OpenBlock.UCanvas.Text_set_text_opacity"] = "设置 %1 文本组件(Text)透明度为 %2";
Blockly.Msg["OpenBlock.UCanvas.Text_set_alignment"] = "设置 %1 文本组件(Text)对齐方式为 %2";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_UL"] = "左上";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_UC"] = "中上";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_UR"] = "右上";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_ML"] = "左中";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_MC"] = "正中";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_MR"] = "右中";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_LL"] = "左下";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_LC"] = "中下";
Blockly.Msg["unityengine_gameobject_text_set_alignment_opt_LR"] = "右下";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_text_fade_and_wait"] = "%1 文本组件(Text)在 %2 秒内将透明比例改为 %3 [完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_text_fade_and_message"] = "%1 文本组件(Text)在 %2 秒内将透明比例改为 %3 完成后发送 %5 消息 附加信息 %4";
Blockly.Msg["OpenBlock.UCanvas.Button_set_interactable"] = "设置 %1 按钮组件(Button)可交互状态为 %2";
Blockly.Msg["OpenBlock.UCanvas.Inputfield_set_interactable"] = "设置 %1 输入框组件(Input Field)可交互状态为 %2";
Blockly.Msg["OpenBlock.UCanvas.Inputfield_set_text"] = "设置 %1 输入框组件(Input Field)文本内容为 %2";
Blockly.Msg["OpenBlock.UCanvas.Inputfield_get_text"] = "%1 输入框组件(Input Field)文本内容";
Blockly.Msg["OpenBlock.UCanvas.Set_scrollbar_value"] = "设置 %1 滚动条组件(Scrollbar)进度为 %2";
Blockly.Msg["OpenBlock.UCanvas.Set_scrollRectHorizontal_value"] = "设置 %1 滑动矩形组件(ScrollRect)%3滑动值为 %2";
Blockly.Msg["OpenBlock.UCanvas.Get_scrollRectHorizontal_value"] = "%1 滑动矩形组件(ScrollRect)水平滑动值";
Blockly.Msg["OpenBlock.UCanvas.Get_ScrollRectVertical_Value"] = "%1 滑动矩形组件(ScrollRect)垂直滑动值";
Blockly.Msg["OpenBlock.UCanvas.Get_scrollRectViewport_RectSize"] = "%1滑动矩形组件(ScrollRect)的%2";
Blockly.Msg["OpenBlock.UCanvas.Set_scrollRect_enable"] = "设置 %1 滑动矩形组件(ScrollRect)激活状态为 %2";
Blockly.Msg["OpenBlock.UCanvas.ScrollRect_EventSystems_OnDrag"] = "%1 滑动矩形组件(ScrollRect)触发事件 鼠标拖动中，信息为%2";
Blockly.Msg["OpenBlock.UCanvas.ScrollRect_EventSystems_OnBeginDrag"] = "%1 滑动矩形组件(ScrollRect)触发事件 鼠标开始拖动，信息为%2";
Blockly.Msg["OpenBlock.UCanvas.ScrollRect_EventSystems_OnEndDrag"] = "%1 滑动矩形组件(ScrollRect)触发事件 鼠标结束拖动，信息为%2";
Blockly.Msg["OpenBlock.UCanvas.Set_slider_value"] = "设置 %1 滑块组件(Slider)进度为 %2";
Blockly.Msg["OpenBlock.UCanvas.Outline_set_color"] = "设置 %1 描边组件(Outline)颜色为 %2";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_get_text"] = "%1 高级文本网格组件(TextMeshPro)文本内容";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_set_color"] = "设置 %1 高级文本网格组件(TextMeshPro)颜色为 %2";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_set_alignment"] = "设置 %1 高级文本网格组件(TextMeshPro)对齐方式为 %2";
Blockly.Msg["unityengine_gameobject_textmeshpro_set_alignment_opt_left"] = "左";
Blockly.Msg["unityengine_gameobject_textmeshpro_set_alignment_opt_center"] = "中";
Blockly.Msg["unityengine_gameobject_textmeshpro_set_alignment_opt_right"] = "右";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_set_font_size"] = "设置 %1 高级文本网格组件(TextMeshPro)字号为 %2";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_set_text"] = "设置 %1 高级文本网格组件(TextMeshPro)文本内容为 %2";
Blockly.Msg["OpenBlock.UCamera.Get_orthographic_size"] = "%1 摄像机组件(Camera)的正交大小";
Blockly.Msg["OpenBlock.UCamera.Set_orthographic_size"] = "设置 %1 摄像机组件(Camera)的正交大小为 %2";
Blockly.Msg["OpenBlock.UScene.Async_get_progress"] = "异步加载 %1 的进度值";
Blockly.Msg["OpenBlock.UScene.Scene_loadscene"] = "加载名称为 %1 的场景";
Blockly.Msg["OpenBlock.UScene.Scene_loadscene_async"] = "异步加载名称为 %1 的场景";
Blockly.Msg["OpenBlock.UScene.Scene_get_loadscene_async"] = "异步加载名称为 %1 的场景";
Blockly.Msg["OpenBlock.UScene.Async_get_is_done"] = "异步加载 %1 是否完成";
Blockly.Msg["OpenBlock.UString.String_copy_to_shearplate"] = "将 %1 复制到剪切板";
Blockly.Msg["OpenBlock.UString.String_starts_with"] = "%1 是否以 %2 开始";
Blockly.Msg["OpenBlock.UString.String_contact"] = "连接文本 %1 %2";
Blockly.Msg["OpenBlock.UString.String_replace"] = "将 %1 中的字符串 %2 替换为 %3";
Blockly.Msg["OpenBlock.UString.String_length"] = "%1 的长度";
Blockly.Msg["OpenBlock.UString.String_remove_string"] = "将 %1 从第 %2 个位置开始移除 %3 个字符";
Blockly.Msg["OpenBlock.UString.String_replace_by_pos"] = "将 %1 第 %2 个字符改为 %3";
Blockly.Msg["OpenBlock.UString.String_split"] = "将 %1 以 %2 分割放入列表";
Blockly.Msg["OpenBlock.UString.String_substring"] = "获取 %1 第 %2 个位置开始 %3 个字符";
Blockly.Msg["OpenBlock.UString.String_upper"] = "%1 转为大写";
Blockly.Msg["OpenBlock.UString.String_new_line"] = "换行符";
Blockly.Msg["OpenBlock.UString.String_escape_uri_string"] = "%1 转为url编码";
Blockly.Msg["OpenBlock.UString.CustomDataTypeStringFormat"] = "自定义类型数据字符串%1格式化";
Blockly.Msg["OpenBlock.UList.List_addrange"] = "将列表 %1 添加到列表 %2 的末尾";
Blockly.Msg["OpenBlock.UList.List_removerange"] = "移除列表 %1 中第 %2 个位置开始 %3 个元素";
Blockly.Msg["OpenBlock.UList.List_remove_target"] = "移除列表 %1 中的 %2 元素";
Blockly.Msg["OpenBlock.UList.List_clear"] = "清空列表 %1";
Blockly.Msg["OpenBlock.UList.List_contains"] = "列表 %1 是否包含元素 %2";
Blockly.Msg["OpenBlock.UList.List_element_showcount"] = "列表 %1 元素 %2 出现次数";
Blockly.Msg["OpenBlock.UList.List_duplicate"] = "复制列表 %1";
Blockly.Msg["OpenBlock.UList.List_to_array"] = "列表 %1 转为列表";
Blockly.Msg["OpenBlock.UArray.Array_new"] = "创建空列表";
Blockly.Msg["OpenBlock.UArray.Array_new_array"] = "创建长度为 %1 的列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_BoxCast"] = "[立方体]中心%1大小%2方向%3角度%4距离%5层%6含触发器%7射线是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Physics_BoxCastOutHit"] = "[立方体]中心%1大小%2方向%3角度%4距离%5层%6含触发器%7射线的碰撞";
Blockly.Msg["OpenBlock.UPhysics.Physics_BoxCastNonAlloc"] = "[立方体]中心%1大小%2方向%3角度%4距离%5层%7碰到的对象数量，输出结果%6";
Blockly.Msg["OpenBlock.UPhysics.Physics_BoxCastOutAllHit"] = "[立方体]中心%1大小%2方向%3角度%4距离%5层%6含触发器%7射线的碰撞列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_BoxCast2d"] = "[矩形]%7%1大小%2角度%3方向%4距离%5层%6射线是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Physics_CapsuleCast"] = "[胶囊]起点%1终点%2半径%3方向%4距离%5层%6含触发器%7射线是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Physics_CapsuleCastHit"] = "[胶囊]起点%1终点%2半径%3方向%4距离%5层%6含触发器%7射线碰撞";
Blockly.Msg["OpenBlock.UPhysics.Physics_CapsuleCastAll"] = "[胶囊]起点%1终点%2半径%3方向%4距离%5层%6含触发器%7射线碰撞列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_CapsuleCastNonAlloc"] = "[胶囊]起点%1终点%2半径%3方向%4距离%5层%7碰到的对象数量，输出结果%6";
Blockly.Msg["OpenBlock.UPhysics.Physics_SphereCastNonAlloc"] = "[球体]中心%1半径%2方向%3距离%4层%6碰到的对象数量，输出结果%5";
Blockly.Msg["OpenBlock.URay.Camera_screenpoint_toray"] = "%1 摄像机组件朝 %2 发射射线";
Blockly.Msg["OpenBlock.URay.Ray_raycast"] = "射线 %1 距离 %2 的碰撞";
Blockly.Msg["OpenBlock.URay.Ray_position_is_raycast_layer"] = "%1 下方向 %2 距离是否有 %3 对象";
Blockly.Msg["OpenBlock.URay.Ray_position_is_circle_raycast_layer_2d"] = "[圆形]中心%1半径%2方向%3距离%4层%5是否发生碰撞？";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit_gameobject"] = "3D射线碰撞 %1 的游戏对象";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit_gameobjects"] = "3D射线碰撞列表 %1 的游戏对象列表";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit_point"] = "3D射线碰撞 %1 的碰撞点坐标";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit_distance"] = "3D射线碰撞 %1 的长度";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit2d_gameobject"] = "2D射线碰撞 %1 的游戏对象";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit2d_gameobjects"] = "2D射线碰撞列表 %1 的游戏对象列表";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit2d_point"] = "2D射线碰撞 %1 的碰撞点坐标";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit2d_distancet"] = "2D射线碰撞 %1 的长度";
Blockly.Msg["OpenBlock.URay.Ray_spherecast_raycasthit"] = "[球体]中心%1半径%2方向%3距离%4层%5含触发器%6的射线碰撞";
Blockly.Msg["OpenBlock.URay.Ray_spherecast_raycasthitAll"] = "[球体]中心%1半径%2方向%3距离%4层%5含触发器%6的射线碰撞列表";
Blockly.Msg["OpenBlock.UPhysics.Ray_is_spherecast_raycasthit"] = "[球体]中心%1半径%2方向%3距离%4层%5含触发器%6是否发生碰撞？";
Blockly.Msg["OpenBlock.URay.Ray_raycast2d_hit"] = "[线]起点%1方向%2距离%3层%4射线的碰撞";
Blockly.Msg["OpenBlock.URay.Physics_BoxCast2d_hit"] = "[矩形]%7%1大小%2角度%3方向%4距离%5层%6射线的碰撞";
Blockly.Msg["OpenBlock.URay.Physics_BoxCast2d_hitAll"] = "[矩形]%7%1大小%2角度%3方向%4距离%5层%6射线的碰撞列表";
Blockly.Msg["OpenBlock.URay.Physics2D_LinecastNonAlloc"] = "[线]起点%1终点%2层%4碰到的对象数量，输出结果%3";
Blockly.Msg["OpenBlock.URay.Physics2D_BoxCastNonAlloc"] = "[矩形]%8%1大小%2角度%3方向%4距离%5层%7碰到的对象数量，输出结果%6";
Blockly.Msg["OpenBlock.URay.Physics2D_CircleNonAlloc"] = "[圆形]中心%1半径%2方向%3距离%4层%6碰到的对象数量，输出结果%5";
Blockly.Msg["OpenBlock.URay.Physics2D_CapsuleCastNonAlloc"] = "[胶囊]中心%1大小%2胶囊方向%8角度%3方向%5距离%4层%7碰到的对象数量，输出结果%6";
Blockly.Msg["OpenBlock.URay.Physics_CircleCast2d_hit"] = "[圆形]中心%1半径%2方向%3距离%4层%5射线的碰撞";
Blockly.Msg["OpenBlock.URay.Physics_CircleCast2d_hitAll"] = "[圆形]中心%1半径%2方向%3距离%4层%5射线的碰撞列表";
Blockly.Msg["OpenBlock.URay.Ray_is_capsuleCast_2d"] = "[胶囊]中心%1大小%2胶囊方向%7角度%3方向%5距离%4层%6是否发生碰撞？";
Blockly.Msg["OpenBlock.URay.Ray_is_capsuleCast_2d_hit"] = "[胶囊]中心%1大小%2胶囊方向%7角度%3方向%5距离%4层%6射线的碰撞";
Blockly.Msg["OpenBlock.URay.Ray_is_capsuleCast_2d_hitAll"] = "[胶囊]中心%1大小%2胶囊方向%7角度%3方向%5距离%4层%6射线的碰撞列表";
Blockly.Msg["OpenBlock.URay.Physics_linecast_hit"] = "[线]起点%1终点%2层%3含触发器%4射线的碰撞";
Blockly.Msg["OpenBlock.URay.Physics_linecast"] = "[线]起点%1终点%2层%3含触发器%4是否发生碰撞？";
Blockly.Msg["OpenBlock.URay.Physics_linecast_2d"] = "[线]起点%1终点%2层%3是否发生碰撞？";
Blockly.Msg["OpenBlock.URay.Physics_linecast_2d_hit"] = "[线]起点%1终点%2层%3的射线碰撞";
Blockly.Msg["OpenBlock.URay.Physics_linecast_2d_hitAll"] = "[线]起点%1终点%2层%3的射线碰撞列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_Distance2d_distance"] = "2D碰撞器%1与%2的2D碰撞距离信息";
Blockly.Msg["OpenBlock.UPhysics.Physics_collider2d_IsTouching"] = "2D碰撞器%1与%2是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Physics_collider2d_IsTouchingLayers"] = "2D碰撞器%1与层%2是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Physics_ColliderDistance2D_distance"] = "2D碰撞距离信息%1的距离";
Blockly.Msg["OpenBlock.UPhysics.Physics_ColliderDistance2D_normal"] = "2D碰撞距离信息%1的法向量";
Blockly.Msg["OpenBlock.UPhysics.Physics_Gameobject_collider2d"] = "%1的2D碰撞器";
Blockly.Msg["OpenBlock.UPhysics.Physics_Gameobject_collider"] = "%1的3D碰撞器";
Blockly.Msg["OpenBlock.UPhysics.Physics_CheckBox"] = "[立方体]中心%1半径%2角度%3层%4含触发器%5是否发生碰撞？";
Blockly.Msg["queryTriggerInteraction_global"] = "使用全局设置";
Blockly.Msg["queryTriggerInteraction_ignore"] = "否";
Blockly.Msg["queryTriggerInteraction_collide"] = "是";
Blockly.Msg["OpenBlock.UMath.LayerMask_layerToName"] = "索引%1层的名称";
Blockly.Msg["OpenBlock.UMath.LayerMask_layerNumber"] = "索引%1层";
Blockly.Msg["OpenBlock.UMath.LayerMask_notLayerNumber"] = "索引%1外全部层";
Blockly.Msg["OpenBlock.UMath.LayerMask_Alllayer"] = "全部层";
Blockly.Msg["OpenBlock.UMath.LayerMask_notlayer"] = "除%1外全部层";
Blockly.Msg["OpenBlock.UMath.LayerMask_NameToLayer"] = "%1层";
Blockly.Msg["OpenBlock.UMath.LayerMask_GetMask"] = "%1和%2层";
Blockly.Msg["OpenBlock.UPhysics.Physics_colliderToGameObject"] = "3D碰撞器%1的游戏对象";
Blockly.Msg["OpenBlock.UPhysics.Physics_colliderToGameObjects"] = "3D碰撞器列表%1的游戏对象列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_colliderToGameObject_2d"] = "2D碰撞器%1的游戏对象";
Blockly.Msg["OpenBlock.UPhysics.Physics_colliderToGameObjects_2d"] = "2D碰撞器列表%1的游戏对象列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_overlayBox"] = "[立方体]中心%1半径%2角度%3层%4含触发器%5碰到的3D碰撞器列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_overlayBox_nonAlloc"] = "[立方体]中心%1半径%2角度%4层%5含触发器%6碰到的对象数量，输出结果%3";
Blockly.Msg["OpenBlock.UPhysics.Physics_overlapSphere"] = "[球体]中心%1半径%2层%3含触发器%4碰到的3D碰撞器列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_overlapSphere_nonAlloc"] = "[球体]中心%1半径%2层%4含触发器%5碰到的对象数量，输出结果%3";
Blockly.Msg["OpenBlock.UPhysics.Physics_is_overlapSphere"] = "[球体]中心%1半径%2层%3含触发器%4是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Overlay_box_gameobject"] = "[立方体]中心%1半径%2角度%3偏移%4层%5碰到的游戏对象列表";
Blockly.Msg["OpenBlock.UPhysics.Position_overlay_box_gameobject_2d"] = "[矩形]%5 %1长宽%2角度%3层%4碰到的2D碰撞器列表";
Blockly.Msg["OpenBlock.UPhysics.Physics2d_is_overlay_box"] = "[矩形]%5 %1 长宽为 %2 角度%3 层%4 是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Physics2d_overlay_box"] = "[矩形]%5 %1 长宽为 %2 角度%3 层%4 碰到的2D碰撞器";
Blockly.Msg["OpenBlock.UPhysics.Physics2d_overlay_box_nonAlloc"] = "[矩形]%6 %1 长宽为 %2 角度%3 层%5 碰到的对象数量，输出结果%4";
Blockly.Msg["OpenBlock.UPhysics.Physics2d_overlay_circle"] = "[圆形]中心%1半径%2层%3碰到的2D碰撞器";
Blockly.Msg["OpenBlock.UPhysics.Physics2d_overlay_circle_nonAlloc"] = "[圆形]中心%1半径%2层%4碰到的对象数量，输出结果%3";
Blockly.Msg["OpenBlock.UPhysics.Physics_is_overlay_capsule"] = "[胶囊]起点%1终点%2半径%3层%4含触发器%5是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Physics_overlay_capsule"] = "[胶囊]起点%1终点%2半径%3层%4含触发器%5碰到的3D碰撞器列表";
Blockly.Msg["OpenBlock.UPhysics.Physics_overlay_capsule_nonAlloc"] = "[胶囊]起点%1终点%2半径%3层%5含触发器%6碰到的对象数量，输出结果%4";
Blockly.Msg["OpenBlock.UPhysics.Position_is_overlap_circle_2d"] = "[圆形]中心%1半径%2层%3是否发生碰撞？";
Blockly.Msg["OpenBlock.UPhysics.Position_overlap_circle_all_gameobject_2d"] = "[圆形]中心%1半径%2层%3碰到的2D碰撞器列表";
Blockly.Msg["OpenBlock.UPhysics.Position_overlap_sphere_gameobject"] = "[球体]中心%1半径%2层%3碰到的游戏对象列表";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_move_position"] = "%1 的刚体组件(Rigidbody)以每秒 %2 的速度向 %3 移动";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2D_Cast"] = "%1的2D刚体朝方向%2距离%5发射碰撞体射线碰撞到的对象数量，输出结果%3长度%4";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2D_CastContactFilter"] = "%1的2D刚体朝方向%2距离%6筛选器%5发射碰撞体射线碰撞到的对象数量，输出结果%3长度%4";
Blockly.Msg["OpenBlock.UPhysics.Physics2DModule_SetContactFilter2DUseTriggers"] = "设置2d筛选器(ContactFilter2D)%1使用触发器为%2";
Blockly.Msg["OpenBlock.UPhysics.Physics2DModule_SetContactFilter2DSetLayerMask"] = "设置2d筛选器(ContactFilter2D)%1层为%2";
Blockly.Msg["OpenBlock.UPhysics.Physics2DModule_SetContactFilter2DSetDepth"] = "设置2d筛选器(ContactFilter2D)%1最小深度%2最大深度%3";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_move_forward"] = "%1 的刚体组件(Rigidbody)以每秒 %2 的速度向 %3 移动";
Blockly.Msg["unityengine_physics_rigidbody_move_forward_opt_forward"] = "前";
Blockly.Msg["unityengine_physics_rigidbody_move_forward_opt_back"] = "后";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_set_kinematic"] = "%1 的刚体组件(Rigidbody)不受力 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_set_velocity"] = "%1 的刚体组件(Rigidbody)速度矢量为 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_set_velocity_single"] = "%1 的刚体组件(Rigidbody)速度矢量%3为 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_set_drag"] = "%1 的刚体组件(Rigidbody)阻力为 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_move_position"] = "%1 的2D刚体组件(Rigidbody2D)以每秒 %2 的速度向 %3 移动";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_move_forward"] = "%1 的2D刚体组件(Rigidbody2D)以每秒 %2 的速度向 %3 移动";
Blockly.Msg["unityengine_physics_rigidbody2d_move_forward_opt_forward"] = "前";
Blockly.Msg["unityengine_physics_rigidbody2d_move_forward_opt_back"] = "后";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_set_velocity"] = "%1 的2D刚体组件(Rigidbody2D)速度矢量为 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_set_velocity_single"] = "%1 的2D刚体组件(Rigidbody2D)速度矢量%3为 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_get_velocity"] = "%1 2D刚体组件(Rigidbody2D)的速度 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_set_gravity"] = "%1 2D刚体组件(Rigidbody2D)的重力缩放为 %2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_addForce"] = "%1 刚体组件(Rigidbody)施加力%2模式%3";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_addForce"] = "%1 刚体组件(Rigidbody2D)施加力%2模式%3";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2D_setSimulated"] = "%1 刚体组件(Rigidbody2D)模拟刚体为%2";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody_addForceAtPosition"] = "%1 刚体组件(Rigidbody)在位置%3施加力%2模式%4";
Blockly.Msg["OpenBlock.UPhysics.Rigidbody2d_addForceAtPosition"] = "%1 刚体组件(Rigidbody2D)在位置%3施加力%2模式%4";
Blockly.Msg["unityengine_ForceMode_Force"] = "力";
Blockly.Msg["unityengine_ForceMode_Impulse"] = "脉冲";
Blockly.Msg["unityengine_ForceMode_Acceleration"] = "加速";
Blockly.Msg["unityengine_ForceMode_VelocityChange"] = "速度变化";
Blockly.Msg["OpenBlock.UPhysics.Physics2d_gravity"] = "2D重力";
Blockly.Msg["OpenBlock.UPhysics.Physics_gravity"] = "3D重力";
Blockly.Msg["OpenBlock.UMath.Physics2d_GetLayerCollisionMask"] = "层%1的2D碰撞层";
Blockly.Msg["OpenBlock.UPhysics.Physics2d_setGravity"] = "设置2D重力为%1";
Blockly.Msg["OpenBlock.UPhysics.Physics_setGravity"] = "设置3D重力为%1";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit2d_normal"] = "2D射线碰撞器%1的法向量";
Blockly.Msg["OpenBlock.URay.Ray_raycasthit_normal"] = "3D射线碰撞器%1的法向量";



Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_touched_gameobject"] = "鼠标碰到的游戏对象";
Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_touched_gameobject_in_layer"] = "%1 上鼠标碰到的游戏对象";
Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_touched_gameobject_in_layer_2d"] = "%1 上鼠标碰到的游戏对象(2D)";
Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_world_position_2d"] = "鼠标的世界坐标(2D)";
Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_world_position"] = "鼠标的世界坐标";
Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_screen_position"] = "鼠标的屏幕坐标";
Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_canvas_position"] = "在画布 %1 中鼠标的坐标";
Blockly.Msg["OpenBlock.UInput.GetGuiRaycastObjectsCount"] = "在画布 %1 中鼠标碰到的游戏对象数量";
Blockly.Msg["OpenBlock.UInput.EventsystemCurrentSelectedGameObject"] = "鼠标碰到的UI对象";
Blockly.Msg["OpenBlock.UInput.IsPointerOverGameObject"] = "鼠标是否碰到UI对象";
//输入-虚拟输入
Blockly.Msg["OpenBlock.UInput.Input_getAxis"] = "虚拟输入%1的值(后续改成下拉菜单选)";
Blockly.Msg["OpenBlock.UInput.Input_getAxisRaw"] = "虚拟输入%1的非平滑值";
Blockly.Msg["OpenBlock.UInput.Input_getButton"] = "虚拟输入%1按住";
Blockly.Msg["OpenBlock.UInput.Input_getButtonDown"] = "虚拟输入%1按下";
Blockly.Msg["OpenBlock.UInput.Input_getButtonUp"] = "虚拟输入%1抬起";
//导航网格
//基础
Blockly.Msg["OpenBlock.UNavmesh.Navmesh_AllAreas"] = "全部区域";
Blockly.Msg["OpenBlock.UNavmesh.Navmesh_CalculatePath"] = "起点%1终点%2区域%3的路径";
Blockly.Msg["OpenBlock.UNavmesh.Navmesh_PathCorners"] = "路径%1的路点位置列表";
Blockly.Msg["OpenBlock.UNavmesh.Navmesh_PathStatus"] = "路径%1是否通畅？";
//导航网格代理
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_UpdatePosition"] = "%1导航代理组件是否自动更新位置？";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setUpdatePosition"] = "设置%1导航代理组件自动更新位置%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setUpdateRotation"] = "设置%1导航代理组件自动更新角度%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setUpdateUpAxis"] = "设置%1导航代理组件自动更新轴%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_NextPosition"] = "%1导航代理组件的下一个位置";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_HasPath"] = "%1导航代理组件路径是否通畅？";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_isPathStale"] = "%1导航代理组件路径是否发生变化？";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_path"] = "%1导航代理组件的路径";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setMaxSpeed"] = "设置%1导航代理组件最大速度为%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setDestinationTo"] = "设置%1导航代理组件目的地为%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_getDestination"] = "%1导航代理组件目的地";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setDestination"] = "设置%1导航代理组件目的地为%2，是否成功？";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_pathPending"] = "%1导航代理组件是否在寻路中？";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_move"] = "%1导航代理组件移动%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_warp"] = "设置%1导航代理组件传送到%2，是否成功？";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setAreaMask"] = "设置%1导航代理组件区域为%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_CalculatePath"] = "%1到%2的路径";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setRadius"] = "设置%1导航代理组件的检测半径为%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_getRadius"] = "%1导航代理组件的检测半径";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_resetPath"] = "%1导航代理组件清空路径";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_isStop"] = "%1导航代理组件停止移动%2";
Blockly.Msg["OpenBlock.UNavmesh.NavmeshAgent_setVelocity"] = "%1导航代理组件速度为%2";

Blockly.Msg["OpenBlock.UScreen.Get_screen_bounce"] = "%1 屏幕边缘坐标";
Blockly.Msg["unityengine_screen_get_screen_bounce_opt_up"] = "上";
Blockly.Msg["unityengine_screen_get_screen_bounce_opt_down"] = "下";
Blockly.Msg["unityengine_screen_get_screen_bounce_opt_left"] = "左";
Blockly.Msg["unityengine_screen_get_screen_bounce_opt_right"] = "右";
Blockly.Msg["OpenBlock.UScreen.Get_screen_size_single"] = "屏幕 %1";
Blockly.Msg["unityengine_screen_get_screen_size_single_opt_height"] = "高度";
Blockly.Msg["unityengine_screen_get_screen_size_single_opt_width"] = "宽度";
Blockly.Msg["OpenBlock.UScreen.Set_screen_auto_rotate"] = "设置自动 %2 为 %1";
Blockly.Msg["unityengine_screen_set_screen_auto_rotate_opt_top"] = "朝上旋转";
Blockly.Msg["unityengine_screen_set_screen_auto_rotate_opt_down"] = "朝下旋转";
Blockly.Msg["unityengine_screen_set_screen_auto_rotate_opt_left"] = "朝左旋转";
Blockly.Msg["unityengine_screen_set_screen_auto_rotate_opt_right"] = "朝右旋转";
Blockly.Msg["OpenBlock.UScreen.Set_screen_orientation"] = "设置屏幕旋转状态为 %1";
Blockly.Msg["unityengine_screen_set_screen_orientation_opt_auto"] = "自动旋转";
Blockly.Msg["unityengine_screen_set_screen_orientation_opt_top"] = "竖屏朝上";
Blockly.Msg["unityengine_screen_set_screen_orientation_opt_down"] = "竖屏朝下";
Blockly.Msg["unityengine_screen_set_screen_orientation_opt_left"] = "横屏朝左";
Blockly.Msg["unityengine_screen_set_screen_orientation_opt_right"] = "横屏朝右";
Blockly.Msg["OpenBlock.UApplication.Application_exit"] = "退出程序";
Blockly.Msg["OpenBlock.UApplication.Application_exit_and_kill_process"] = "退出程序并结束进程";
Blockly.Msg["OpenBlock.UApplication.Unity_editor_stop_playing"] = "停止运行编辑器";
Blockly.Msg["OpenBlock.UApplication.Unity_editor_pause"] = "暂停编辑器";
Blockly.Msg["OpenBlock.UApplication.Runtime_platform_MacAddress"] = "Mac地址";
Blockly.Msg["OpenBlock.UApplication.Runtime_platform_Unique_Id"] = "设备唯一标识";
Blockly.Msg["OpenBlock.UApplication.Get_runtime_platform"] = "程序运行在 %1 系统上";
Blockly.Msg["unityengine_application_get_runtime_platform_opt_WindowsPlayer"] = "windows";
Blockly.Msg["unityengine_application_get_runtime_platform_opt_Android"] = "安卓";
Blockly.Msg["unityengine_application_get_runtime_platform_opt_IPhonePlayer"] = "IOS";
Blockly.Msg["unityengine_application_get_runtime_platform_opt_WebGLPlayer"] = "网页";
Blockly.Msg["unityengine_application_get_runtime_platform_opt_WindowsEditor"] = "windows编辑器";
Blockly.Msg["unityengine_application_get_runtime_platform_opt_XboxOne"] = "XboxOne";
Blockly.Msg["unityengine_application_get_runtime_platform_opt_PS4"] = "PS4";
Blockly.Msg["OpenBlock.UApplication.Runtime_platform_ismobile"] = "程序是否运行在移动设备上";
Blockly.Msg["OpenBlock.UApplication.Get_storage_directory"] = "本地储存目录路径";
Blockly.Msg["OpenBlock.UResourcesLoad.Get_file_exists"] = "Resources/%1是否存在文件?";
Blockly.Msg["OpenBlock.UResourcesLoad.ResourcesLoadObject"] = "Resources/%1加载文件";
Blockly.Msg["OpenBlock.UApplication.Application_never_sleep"] = "设置屏幕不自动休眠";
//系统-设备信息
Blockly.Msg["OpenBlock.UApplication.SystemInfo_batteryLevel"] = "电池电量";
Blockly.Msg["OpenBlock.UApplication.SystemInfo_deviceModle"] = "设备型号";
Blockly.Msg["OpenBlock.UApplication.SystemInfo_deviceName"] = "设备名称";
Blockly.Msg["OpenBlock.UApplication.SystemInfo_graphicsDeviceID"] = "显卡标识符代码";
Blockly.Msg["OpenBlock.UApplication.SystemInfo_graphicsDeviceVendorID"] = "显卡供应商标识符代码";
Blockly.Msg["OpenBlock.UApplication.SystemInfo_processorCount"] = "CPU逻辑核心数";
Blockly.Msg["OpenBlock.UApplication.SystemInfo_supportsGyroscope"] = "是否支持陀螺仪";
//Renderer材质
Blockly.Msg["OpenBlock.URenderer.SetMaterialMainTextureColor"] = "%1的渲染组件(Renderer)着色器，颜色类型变量%2设置为%3";
Blockly.Msg["OpenBlock.URenderer.SetMaterialsMainTextureColor"] = "%1的子节点(含自己)的渲染组件(Renderer)着色器，颜色类型变量%2设置为%3";
Blockly.Msg["OpenBlock.URenderer.SetMaterialMainTextureInt"] = "%1的渲染组件(Renderer)着色器，整型类型变量%2设置为%3";
Blockly.Msg["OpenBlock.URenderer.SetMaterialMainTextureTexture"] = "%1的渲染组件(Renderer)着色器，贴图类型变量%2设置为%3";
Blockly.Msg["OpenBlock.URenderer.SetMaterialMainTextureFloat"] = "%1的渲染组件(Renderer)着色器，浮点类型变量%2设置为%3";
Blockly.Msg["OpenBlock.URenderer.GetMaterialMainTextureColor"] = "%1的渲染组件(Renderer)着色器，颜色类型变量%2值";
Blockly.Msg["OpenBlock.URenderer.GetMaterialMainTextureFloat"] = "%1的渲染组件(Renderer)着色器，浮点类型变量%2值";
Blockly.Msg["OpenBlock.URenderer.GetMaterialMainTextureTexture"] = "%1的渲染组件(Renderer)着色器，贴图类型变量%2值";
Blockly.Msg["OpenBlock.URenderer.GetMaterialMainTextureInt"] = "%1的渲染组件(Renderer)着色器，整型类型变量%2值";

Blockly.Msg["OpenBlock.UScreen.Set_screen_resolution"] = "设置屏幕分辨率 宽 %1 高 %2";
Blockly.Msg["OpenBlock.UAudio.Audio_get_length"] = "%1 音频组件(Audio)当前音频时长";
Blockly.Msg["OpenBlock.UAudio.Load_audioclip_from_resources"] = "从资源目录 %1 加载音频文件";
Blockly.Msg["OpenBlock.UAudio.Play_audio"] = "%1 音频组件(Audio)开始播放音频";
Blockly.Msg["OpenBlock.UAudio.Play_audio_at_point"] = "在 %1 播放音频 %2 ";
Blockly.Msg["OpenBlock.UAudio.Set_audio_loop"] = "%1 音频组件(Audio)循环播放为 %2";
Blockly.Msg["OpenBlock.UAudio.Set_audioclip_path_form_resources"] = "%1 音频组件(Audio)来源音效为%2";
Blockly.Msg["OpenBlock.UAudio.Stop_audio_play"] = "%1 音频组件(Audio)停止播放音频";
Blockly.Msg["OpenBlock.UDatetime.Current_time"] = "客户端当前日期";
Blockly.Msg["OpenBlock.UDatetime.Current_milsec_time"] = "客户端当前日期毫秒字符串";
Blockly.Msg["OpenBlock.UDatetime.Server_current_milsec_time"] = "服务器当前日期毫秒字符串";
Blockly.Msg["OpenBlock.UDatetime.Server_current_time"] = "服务器当前日期";
Blockly.Msg["OpenBlock.UDatetime.Date_to_milsec"] = "%1 转为毫秒字符串";
Blockly.Msg["OpenBlock.UDatetime.Milsec_to_date"] = "毫秒字符串 %1 转为日期";
Blockly.Msg["OpenBlock.UDatetime.Date_input"] = "%1 年 %2 月 %3 日 %4 时 %5 分 %6 秒";
Blockly.Msg["OpenBlock.UDatetime.String_to_datetime"] = "%1 转为日期类型";
Blockly.Msg["OpenBlock.UDatetime.Datetime_to_string"] = "%1 转为字符串";
Blockly.Msg["OpenBlock.UDatetime.Days_from_end_month"] = "%1 距离月底还有几天";
Blockly.Msg["OpenBlock.UDatetime.Get_datetime_value"] = "%1 %2";
Blockly.Msg["unityengine_datetime_get_datetime_value_opt_Year"] = "年";
Blockly.Msg["unityengine_datetime_get_datetime_value_opt_Month"] = "月";
Blockly.Msg["unityengine_datetime_get_datetime_value_opt_Day"] = "日";
Blockly.Msg["unityengine_datetime_get_datetime_value_opt_Hour"] = "时";
Blockly.Msg["unityengine_datetime_get_datetime_value_opt_Minute"] = "分";
Blockly.Msg["unityengine_datetime_get_datetime_value_opt_Second"] = "秒";
Blockly.Msg["unityengine_datetime_get_datetime_value_opt_Millisecond"] = "毫秒";
Blockly.Msg["OpenBlock.UDatetime.Change_format"] = "%1 %2 字符串";
Blockly.Msg["unityengine_datetime_change_format_opt_12"] = "12小时制";
Blockly.Msg["unityengine_datetime_change_format_opt_24"] = "24小时制";
Blockly.Msg["OpenBlock.UDatetime.Datetime_compare"] = "%1 %3 %2";
Blockly.Msg["OpenBlock.UDatetime.Datetime_arithmetic"] = "%1 与 %2 相差多少 %3";
Blockly.Msg["unityengine_datetime_arithmetic_opt_year"] = "年";
Blockly.Msg["unityengine_datetime_arithmetic_opt_month"] = "月";
Blockly.Msg["unityengine_datetime_arithmetic_opt_day"] = "日";
Blockly.Msg["unityengine_datetime_arithmetic_opt_hour"] = "时";
Blockly.Msg["unityengine_datetime_arithmetic_opt_minute"] = "分";
Blockly.Msg["unityengine_datetime_arithmetic_opt_second"] = "秒";
Blockly.Msg["unityengine_datetime_arithmetic_opt_milliseconds"] = "毫秒";
Blockly.Msg["OpenBlock.UDatetime.Datetime_arithmetic_natural"] = "%1 与 %2 相差多少 %3";
Blockly.Msg["unityengine_datetime_arithmetic_natural_opt_year"] = "自然年";
Blockly.Msg["unityengine_datetime_arithmetic_natural_opt_month"] = "自然月";
Blockly.Msg["unityengine_datetime_arithmetic_natural_opt_week"] = "自然星期";
Blockly.Msg["unityengine_datetime_arithmetic_natural_opt_day"] = "自然日";
Blockly.Msg["OpenBlock.UDatetime.Datetime_to_week"] = "%1 是星期几";
Blockly.Msg["OpenBlock.UNativeaudio.Load_audio"] = "预加载音效 %1";
Blockly.Msg["OpenBlock.UNativeaudio.Play_audio"] = "播放预加载音效 %1";
Blockly.Msg["OpenBlock.UNativeaudio.Unload_audio"] = "卸载预加载音效 %1";
Blockly.Msg["OpenBlock.USharesdk.Share_image_wechat"] = "%1 sharesdk组件将屏幕截图分享给微信好友";
Blockly.Msg["OpenBlock.USharesdk.Share_image_wechat_moment"] = "%1 sharesdk组件将屏幕截图分享给微信朋友圈";
Blockly.Msg["OpenBlock.USharesdk.Share_website_wechat"] = "%1 sharesdk组件分享链接 %2 标题 %3 内容 %4 给微信好友";
Blockly.Msg["OpenBlock.USharesdk.Share_website_wechat_moment"] = "%1 sharesdk组件分享链接 %2 标题 %3 内容 %4 到微信朋友圈";
Blockly.Msg["OpenBlock.USpine.Set_animation"] = "%1 spine动画组件播放动作 %2 循环播放 %3";
Blockly.Msg["OpenBlock.USpine.Set_order"] = "%1 spine动画组件图层顺序为 %2";
Blockly.Msg["OpenBlock.USpine.Set_skeleton"] = "%1 spine动画组件动画文件为 %2";
Blockly.Msg["OpenBlock.USpine.Set_skeleton_from_res"] = "%1 spine动画组件从资源路径 %2 加载动画文件";
Blockly.Msg["OpenBlock.USpine.Skeleton_init"] = "%1 spine动画组件刷新显示";
Blockly.Msg["OpenBlock.USpine.Skeleton_set_skin"] = "设置 %1 spine动画组件皮肤为 %2";
Blockly.Msg["OpenBlock.USpine.Skeleton_set_sortlayer"] = "设置 %1 spine动画组件排序图层为 %2";
Blockly.Msg["OpenBlock.USpine.Skeleton_ugui_set_animation"] = "%1 spineUGUI动画组件播放动作 %2 循环播放 %3";
Blockly.Msg["OpenBlock.UTalkingdata.Talkingdata_init"] = "初始化数据统计,APPID%1,渠道名%2";
Blockly.Msg["OpenBlock.UTalkingdata.Get_device_id"] = "设备唯一ID";
Blockly.Msg["OpenBlock.UTalkingdata.Get_account"] = "识别码为 %1 的账户";
Blockly.Msg["OpenBlock.UTalkingdata.Set_account_type"] = "设置账户 %1 类型为 %2";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_ANONYMOUS"] = "匿名";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_REGISTERED"] = "自有帐户显性注册";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_WEIXIN"] = "微信";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_QQ"] = "QQ";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_SINA_WEIBO"] = "新浪微博";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_QQ_WEIBO"] = "腾讯微博";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_TYPE1"] = "自定义类型1";
Blockly.Msg["unityengine_talkingdata_set_account_type_opt_TYPE2"] = "自定义类型2";
Blockly.Msg["OpenBlock.UTalkingdata.Set_account_name"] = "设置账户 %1 名称为 %2";
Blockly.Msg["OpenBlock.UTalkingdata.Set_account_level"] = "设置账户 %1 等级为 %2";
Blockly.Msg["OpenBlock.UTalkingdata.Set_account_age"] = "设置账户 %1 年龄为 %2";
Blockly.Msg["OpenBlock.UTalkingdata.Set_account_gameserver"] = "设置账户 %1 分区名称为 %2";
Blockly.Msg["OpenBlock.UTalkingdata.Set_account_gender"] = "设置账户 %1 性别为 %2";
Blockly.Msg["unityengine_talkingdata_set_account_gender_opt_male"] = "男";
Blockly.Msg["unityengine_talkingdata_set_account_gender_opt_female"] = "女";
Blockly.Msg["unityengine_talkingdata_set_account_gender_opt_unknown"] = "未知";
Blockly.Msg["OpenBlock.UTalkingdata.On_charge_request"] = "申请支付订单ID %1 商品名 %2 金额 %3 币种 %6 虚拟币金额 %4 支付方式 %5";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_CNY"] = "人民币";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_USD"] = "美元";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_EUR"] = "欧元";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_JPY"] = "日元";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_GBP"] = "英镑";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_HKD"] = "港元";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_TWD"] = "新台币";
Blockly.Msg["unityengine_talkingdata_on_charge_request_opt_DIY"] = "其他";
Blockly.Msg["OpenBlock.UTalkingdata.On_charge_success"] = "订单ID %1 支付成功";
Blockly.Msg["OpenBlock.UTalkingdata.On_purchase"] = "使用虚拟币以 %1 单价购买 %2 个 %3 物品";
Blockly.Msg["OpenBlock.UTalkingdata.Reward"] = "从 %1 中获取虚拟币 %2 个";
Blockly.Msg["OpenBlock.UTalkingdata.On_use"] = "消耗 %1 个 %2 物品";
Blockly.Msg["OpenBlock.UTalkingdata.Mission_begin"] = "任务(关卡) %1 开始";
Blockly.Msg["OpenBlock.UTalkingdata.Mission_completed"] = "任务(关卡) %1 完成";
Blockly.Msg["OpenBlock.UTalkingdata.Mission_failed"] = "任务(关卡) %1 失败，原因 %2";
Blockly.Msg["OpenBlock.UDOTween.Gmeobject_rotate_and_message"] = "%1 在 %2 秒内旋转到 %3 欧拉角(世界)完成后发送 %5 消息并附加信息 %4";
Blockly.Msg["OpenBlock.ULineRenderer.Gameobject_linerenderer_set_shared_material_maintexture_offset"] = "设置 %1 线渲染器组件(Line Renderer)共享材质主纹理偏移为 %2";
Blockly.Msg["OpenBlock.ULineRenderer.Gameobject_linerenderer_set_shared_material_maintexture_scale"] = "设置 %1 线渲染器组件(Line Renderer)共享材质主纹理缩放为 %2";
Blockly.Msg["OpenBlock.UApplication.Application_run_in_background"] = "允许应用程序在后台运行 %1";
Blockly.Msg["OpenBlock.UApplication.CursorVisible"] = "设置鼠标可见为 %1";
Blockly.Msg["OpenBlock.USharesdk.On_share_success"] = "分享成功";
Blockly.Msg["OpenBlock.USharesdk.On_share_fail"] = "分享失败";
Blockly.Msg["OpenBlock.USharesdk.On_share_cancel"] = "取消分享";
Blockly.Msg["OpenBlock.USpine.On_spine_complete"] = "动画播放结束";
Blockly.Msg["OpenBlock.USpine.On_spine_complete_dv"] = "动画播放结束的动画名";
Blockly.Msg["OpenBlock.USpine.On_spine_end"] = "动画播放中断";
Blockly.Msg["OpenBlock.USpine.On_spine_end_dv"] = "动画播放中断的动画名";
Blockly.Msg["OpenBlock.USpine.On_spine_event"] = "动画触发事件";
Blockly.Msg["OpenBlock.USpine.On_spine_event_dv"] = "动画触发的事件信息";
Blockly.Msg["OpenBlock.USpine.On_spine_event_name"] = "事件名称";
Blockly.Msg["OpenBlock.USpine.On_spine_event_number"] = "事件数值参数";
Blockly.Msg["OpenBlock.USpine.On_spine_event_string"] = "事件字符串参数";
Blockly.Msg["OpenBlock.UEvent.On_gameobject_destory"] = "游戏对象被销毁";
Blockly.Msg["OpenBlock.UEvent.On_network_close"] = "网络断开";
Blockly.Msg["OpenBlock.UEvent.On_network_open"] = "网络连接";
Blockly.Msg["unityengine_event_on_network_reconnect_fail"] = "网络重连失败";
Blockly.Msg["unityengine_event_on_network_reconnect_success"] = "网络重连成功";
Blockly.Msg["OpenBlock.UEvent.On_gameobject_enable"] = "初始化";
Blockly.Msg["OpenBlock.UEvent.On_gameobject_start"] = "对象初始化";
Blockly.Msg["OpenBlock.UEvent.On_gameobject_state_enter"] = "进入状态";
Blockly.Msg["OpenBlock.UEvent.On_collision_enter_2d"] = "碰撞进入(2D)";
Blockly.Msg["OpenBlock.UEvent.On_collision_enter"] = "碰撞进入";
Blockly.Msg["OpenBlock.UEvent.On_collision_enter_dequeue_value_2d"] = "获取进入的碰撞器(2D)";
Blockly.Msg["OpenBlock.UEvent.On_collision_enter_dequeue_value"] = "获取进入的碰撞器";
Blockly.Msg["OpenBlock.UEvent.On_collision_stay_dequeue_value"] = "获取停留的碰撞器";
Blockly.Msg["OpenBlock.UEvent.On_collision_exit_dequeue_value"] = "获取离开的碰撞器";
Blockly.Msg["OpenBlock.UEvent.On_collision_stay_dequeue_value_2d"] = "获取停留的碰撞器(2D)";
Blockly.Msg["OpenBlock.UEvent.On_collision_exit_dequeue_value_2d"] = "获取离开的碰撞器(2D)";
Blockly.Msg["OpenBlock.UEvent.Collision_gameobject_2d"] = "获取碰撞器(2D) %1 的游戏对象";
Blockly.Msg["OpenBlock.UEvent.Collision_gameobject"] = "获取碰撞器 %1 的游戏对象";
Blockly.Msg["OpenBlock.UEvent.Collision_position"] = "获取碰撞器 %1 的三维坐标(世界)";
Blockly.Msg["OpenBlock.UEvent.Collision_ContactsNormal"] = "获取碰撞器 %1 的法向量";
Blockly.Msg["OpenBlock.UEvent.On_trigger_enter_2d"] = "碰撞触发器进入(2D)";
Blockly.Msg["OpenBlock.UEvent.On_trigger_exit_2d"] = "碰撞触发器离开(2D)";
Blockly.Msg["OpenBlock.UEvent.On_trigger_enter"] = "碰撞触发器进入";
Blockly.Msg["OpenBlock.UEvent.On_trigger_exit"] = "碰撞触发器离开";
Blockly.Msg["OpenBlock.UEvent.On_trigger_stay"] = "碰撞触发器停留";
Blockly.Msg["OpenBlock.UEvent.On_trigger_stay_dequeue_value_2d"] = "获取停留的触发器信息(2D)";
Blockly.Msg["OpenBlock.UEvent.On_trigger_enter_dequeue_value_2d"] = "获取进入的触发器信息(2D)";
Blockly.Msg["OpenBlock.UEvent.On_trigger_exit_dequeue_value"] = "获取离开的触发器信息";
Blockly.Msg["OpenBlock.UEvent.On_trigger_stay_dequeue_value"] = "获取停留的触发器信息";
Blockly.Msg["OpenBlock.UEvent.On_trigger_enter_dequeue_value"] = "获取进入的触发器信息";
Blockly.Msg["OpenBlock.UEvent.On_trigger_exit_dequeue_value_2d"] = "获取离开的触发器信息(2D)";
Blockly.Msg["OpenBlock.UEvent.Collider_gameobject_2d"] = "获取触发器信息(2D) %1 的游戏对象";
Blockly.Msg["OpenBlock.UEvent.Collider_gameobject_tag_2d"] = "获取触发器信息(2D) %1 的标签";
Blockly.Msg["OpenBlock.UEvent.Collider_gameobject"] = "获取触发器信息 %1 的游戏对象";
Blockly.Msg["OpenBlock.UEvent.Collider_gameobject_tag"] = "获取触发器信息 %1 的标签";
Blockly.Msg["OpenBlock.UEvent.CollisionContactCount"] = "获取碰撞器 %1 的碰撞点数量";
Blockly.Msg["OpenBlock.UEvent.CollisionGetContact"] = "获取碰撞器 %1 索引%2的碰撞点信息";
Blockly.Msg["OpenBlock.UEvent.ContactPointPoint"] = "获取碰撞点信息 %1 的坐标";
Blockly.Msg["OpenBlock.UEvent.ContactPointNormal"] = "获取碰撞点信息 %1 的法向量";
Blockly.Msg["OpenBlock.UEvent.Collision2DContactCount"] = "获取碰撞器(2D) %1 的碰撞点数量";
Blockly.Msg["OpenBlock.UEvent.Collision2DGetContact"] = "获取碰撞器(2D) %1 索引%2的碰撞点信息";
Blockly.Msg["OpenBlock.UEvent.ContactPoint2DPoint"] = "获取碰撞点信息(2D) %1 的坐标";
Blockly.Msg["OpenBlock.UEvent.ContactPoint2DNormal"] = "获取碰撞点信息(2D) %1 的法向量";
Blockly.Msg["OpenBlock.UEvent.PointerEventData_PointerID"] = "鼠标事件数据 %1 的指针ID";
Blockly.Msg["OpenBlock.UEvent.PointerEventData_Position"] = "鼠标事件数据 %1 的坐标";
Blockly.Msg["OpenBlock.UEvent.PointerEventData_Delta"] = "鼠标事件数据 %1 的增量坐标";
Blockly.Msg["OpenBlock.UEvent.PointerEventData_PressPosition"] = "鼠标事件数据 %1 的起始坐标";
Blockly.Msg["OpenBlock.UPayEvent.Payfail"] = "支付失败";
Blockly.Msg["OpenBlock.UPayEvent.Payorder_create"] = "创建支付订单";
Blockly.Msg["OpenBlock.UPayEvent.Payorder_finished"] = "完成支付订单";
Blockly.Msg["OpenBlock.UPayEvent.Paysuccess"] = "支付成功";
Blockly.Msg["OpenBlock.UPayEvent.Get_payattach"] = "支付成功附加数据";
Blockly.Msg["OpenBlock.UPayEvent.Get_pay_orderid"] = "%1 ID";
Blockly.Msg["openblock_pay_get_pay_orderid_opt_create"] = "订单创建";
Blockly.Msg["openblock_pay_get_pay_orderid_opt_finish"] = "订单完成";
Blockly.Msg["OpenBlock.UPayEvent.Get_paytype"] = "支付成功支付类型";
Blockly.Msg["OpenBlock.UPayEvent.PayCreatedOrderID"] = "创建的订单ID";
Blockly.Msg["OpenBlock.UPayEvent.PayCreatedPayType"] = "创建的订单支付类型";
Blockly.Msg["OpenBlock.UPayEvent.PaySuccessOrderID"] = "支付成功的订单ID";
Blockly.Msg["OpenBlock.UPayEvent.PaySuccessPayType"] = "支付成功的订单支付类型";
Blockly.Msg["OpenBlock.UPayEvent.PayFailOrderID"] = "支付失败的订单ID";
Blockly.Msg["OpenBlock.UPayEvent.PayFailPayType"] = "支付失败的订单支付类型";

Blockly.Msg["OpenBlock.UMessage.Send_message"] = "发送消息 %1 给 %2 并附加信息 %3";
Blockly.Msg["OpenBlock.UMessage.Wait_and_set_current_message"] = "等待直到收到 %1 消息";
Blockly.Msg["OpenBlock.UMessage.Send_message_to_children"] = "发送消息 %3 给 %1 及其子节点，并附加信息 %2 包含自己 %4";
Blockly.Msg["OpenBlock.UMessage.Set_current_message"] = "收到 %1 消息";
Blockly.Msg["OpenBlock.UMessage.Get_send_gameobject"] = "当前消息的来源游戏对象";
Blockly.Msg["OpenBlock.UMessage.Broadcast_send_message"] = "广播 %1 并附加信息 %2";
Blockly.Msg["OpenBlock.UMessage.Clear_messages"] = "清空全部消息";
Blockly.Msg["OpenBlock.UMessage.Clear_message_by_title"] = "清除 %1 消息";
Blockly.Msg["OpenBlock.UMessage.Get_current_message_attachment"] = "附加信息";
Blockly.Msg["OpenBlock.UMessage.Set_current_message_dont_clear"] = "收到 %1 消息(保留消息)";
Blockly.Msg["OpenBlock.UNetwork.New_request_data"] = "新建请求空数据对象";
Blockly.Msg["OpenBlock.UNetwork.No_network"] = "无网络连接";
Blockly.Msg["OpenBlock.UNetwork.Get_response_field"] = "获取返回结果 %2 的 %1";
Blockly.Msg["openblock_network_get_response_field_opt_statecode"] = "状态码";
Blockly.Msg["openblock_network_get_response_field_opt_message"] = "附加信息";
Blockly.Msg["OpenBlock.UNetwork.Get_next_number"] = "获取自增数";
Blockly.Msg["OpenBlock.UNetwork.Open_url"] = "打开网址 %1";
Blockly.Msg["OpenBlock.UNetwork.Version_compare_version"] = "对比 %1 与 %2 版本号差异";
Blockly.Msg["OpenBlock.UNetwork.Get_current_version"] = "当前版本号";
Blockly.Msg["OpenBlock.UNetwork.Get_cached_last_version"] = "缓存的最新版本号";
Blockly.Msg["OpenBlock.UNetwork.Get_last_version"] = "最新版本号";
Blockly.Msg["OpenBlock.UNetwork.Get_version_url"] = "获取下载地址";
Blockly.Msg["OpenBlock.UNetwork.User_is_login"] = "是否已登录";
Blockly.Msg["OpenBlock.UNetwork.User_login_req_state"] = "登录状态请求中";
Blockly.Msg["OpenBlock.UNetwork.Get_user_info"] = "获取用户的 %1";
Blockly.Msg["openblock_network_user_get_user_info_opt_userId"] = "用户ID";
Blockly.Msg["openblock_network_user_get_user_info_opt_channel"] = "渠道";
Blockly.Msg["openblock_network_user_get_user_info_opt_channelUserId"] = "渠道用户ID";
Blockly.Msg["openblock_network_user_get_user_info_opt_registedTime"] = "注册时间";
Blockly.Msg["openblock_network_user_get_user_info_opt_lastLoginTime"] = "最后登录时间";
Blockly.Msg["OpenBlock.UNetwork.User_register"] = "注册新账号 用户名 %1 密码 %2";
Blockly.Msg["OpenBlock.UNetwork.User_verification_register"] = "注册新账号 用户名 %1 密码 %2 验证码 %3";
Blockly.Msg["OpenBlock.UNetwork.User_logout"] = "退出登录";
Blockly.Msg["OpenBlock.UNetwork.User_change_password_offline"] = "修改密码 用户名 %1 原密码 %2 新密码 %3";
Blockly.Msg["OpenBlock.UNetwork.User_change_password_online"] = "修改当前用户密码 原密码 %1 新密码 %2";
Blockly.Msg["OpenBlock.UNetwork.User_login_common"] = "以用户名 %1 密码 %2 登录，保留登录状态 %3";
Blockly.Msg["OpenBlock.UNetwork.User_login_auth_code"] = "以用户名 %1 验证码 %2 登录，保留登录状态 %3";
Blockly.Msg["OpenBlock.UNetwork.User_get_auth_code"] = "获取账号 %1 验证码";
Blockly.Msg["OpenBlock.UNetwork.User_judge_user_exist"] = "账号%1是否存在";
Blockly.Msg["OpenBlock.UNetwork.Location_get_info"] = "位置信息";
Blockly.Msg["OpenBlock.UNetwork.Location_get_details"] = "位置信息 %1 的 %2";
Blockly.Msg["openblock_network_location_get_details_opt_country"] = "国家";
Blockly.Msg["openblock_network_location_get_details_opt_province"] = "省份";
Blockly.Msg["openblock_network_location_get_details_opt_city"] = "城市";
Blockly.Msg["openblock_network_location_get_details_opt_district"] = "地区";
Blockly.Msg["openblock_network_location_get_details_opt_township"] = "乡镇";
Blockly.Msg["OpenBlock.UDatabase.Start_db_sync"] = "同步用户数据";
Blockly.Msg["OpenBlock.UDatabase.Database_query_all"] = "返回所有结果";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_ugui_get_text"] = "%1 高级文本网格组件(TextMeshProUGUI)文本内容";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_ugui_set_color"] = "设置 %1 高级文本网格组件(TextMeshProUGUI)颜色为 %2";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_ugui_set_alignment"] = "设置 %1 高级文本网格组件(TextMeshProUGUI)对齐方式为 %2";
Blockly.Msg["unityengine_gameobject_textmeshpro_ugui_set_alignment_opt_left"] = "左";
Blockly.Msg["unityengine_gameobject_textmeshpro_ugui_set_alignment_opt_center"] = "中";
Blockly.Msg["unityengine_gameobject_textmeshpro_ugui_set_alignment_opt_right"] = "右";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_ugui_set_font_size"] = "设置 %1 高级文本网格组件(TextMeshProUGUI)字号为 %2";
Blockly.Msg["OpenBlock.UTextmeshpro.Textmeshpro_ugui_set_text"] = "设置 %1 高级文本网格组件(TextMeshProUGUI)文本内容为 %2";
Blockly.Msg["OpenBlock.UAdvertisement.Init"] = "初始化GameID为 %1 的广告平台";
Blockly.Msg["OpenBlock.UAdvertisement.Advertisement_is_showing"] = "广告是否在展示中";
Blockly.Msg["OpenBlock.UAdvertisement.Advertisement_is_ready"] = "广告是否准备完成";
Blockly.Msg["OpenBlock.UAdvertisement.Advertisement_show_ad"] = "展示 %1 广告";
Blockly.Msg["OpenBlock.UAdvertisement.Advertisement_show_ad_and_send_message"] = "展示 %1 广告，完成后发消息 %2 给自己并附加信息(成功、失败、跳过)";
Blockly.Msg["OpenBlock.UAstarpathfinding.Get_vector3_path"] = "%1 移动到 %2 的寻路位置列表";
Blockly.Msg["OpenBlock.UAstarpathfinding.Scan_path"] = "生成地图障碍数据";
Blockly.Msg["OpenBlock.UAstarpathfinding.Set_grid_graph"] = "设置寻路节点Grid Graph 宽 %1 高 %2 中心X %3 中心Y %4 节点大小 %5";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_AstarAI_SetDestination"] = "设置%1寻路AI组件目标位置为%2";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_AstarAI_SetMaxSpeed"] = "设置%1寻路AI组件最大速度为%2";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_AstarAI_SetCanMove"] = "设置%1寻路AI组件允许移动为%2";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_AstarAI_SetCanSearch"] = "设置%1寻路AI组件允许搜索路线为%2";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_AstarAI_SetRadius"] = "设置%1寻路AI组件对象半径为%2";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_AstarAI_SearchPath"] = "%1寻路AI组件搜索路线";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_SetUpdateGraphs"] = "设置%1碰撞体组件加入到网格地图中";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_SetGraphUpdateObjectUpdatePhysics"] = "设置%1碰撞体组件加入到网格地图中,是否重新计算%2";
Blockly.Msg["OpenBlock.UAstarpathfinding.Astar_AddGameObjectCheckPathClear"] = "如果加入%1碰撞体组件，路径%2到%3是否通畅？";
Blockly.Msg["OpenBlock.DragonBone.Get_animation"] = "%1 当前动作名称";
Blockly.Msg["OpenBlock.DragonBone.Set_animation"] = "%1 动作设置为 %2";
Blockly.Msg["OpenBlock.UInput.Keyboard_input"] = "当键盘%1 %2";
Blockly.Msg["OpenBlock.UInput.Keyboard_input_mouse"] = "当鼠标%1 %2";
Blockly.Msg["OpenBlock.UInput.Keyboard_input_Joystick"] = "当手柄%1 %2";
Blockly.Msg["OpenBlock.UInput.InputAcceleration"] = "设备线性加速度";
Blockly.Msg["OpenBlock.UInput.Keyboard_inputString"] = "键盘按下的按键字符串";
Blockly.Msg["OpenBlock.UTouch.Input_touch_pressure"] = "%1的触摸压力";
Blockly.Msg["OpenBlock.UTouch.Touch_delta_rawPosition"] = "%1的起始坐标";
Blockly.Msg["OpenBlock.UTouch.Input_touch_TapCount"] = "%1的触摸次数";
Blockly.Msg["OpenBlock.UTouch.Input_get_currentTouchIndex"] = "当前按下的手指ID";
Blockly.Msg["OpenBlock.UInput.Mouse_get_mouse_mouseScrollDelta"] = "鼠标滚轮增量";




//从文件中加载资源
Blockly.Msg["OpenBlock.UResourcesLoad.ResourcesLoadMaterial"] = "Resources/%1加载材质(Material)";
Blockly.Msg["OpenBlock.UResourcesLoad.ResourcesLoadGameObject"] = "Resources/%1加载游戏对象";
Blockly.Msg["OpenBlock.UResourcesLoad.ResourcesLoadImage"] = "Resources/%1加载精灵图片(Sprite)";
Blockly.Msg["OpenBlock.UResourcesLoad.ResourcesLoadAudioClip"] = "Resources/%1加载音频(AudioClip)";
Blockly.Msg["OpenBlock.UResourcesLoad.ResourcesLoadFont"] = "Resources/%1加载字体(Font)";





Blockly.Msg["unityengine_input_keyboard_input_opt"] = [
    ["None", "None"],
    ["退格键", "Backspace"],
    ["Tab", "Tab"],
    ["清除键", "Clear"],
    ["回车键", "Return"],
    ["暂停键", "Pause"],
    ["ESC", "Escape"],
    ["空格键", "Space"],
    ["!", "Exclaim"],
    ["*", "DoubleQuote"],
    ["Hash", "Hash"],
    ["$", "Dollar"],
    ["Ampersand键", "Ampersand"],
    ["'", "Quote"],
    ["(", "LeftParen"],
    [")", "RightParen"],
    ["*", "Asterisk"],
    ["+", "Plus"],
    [",", "Comma"],
    ["-", "Minus"],
    [".", "Period"],
    ["/", "Slash"],
    ["数字键0", "Alpha0"],
    ["数字键1", "Alpha1"],
    ["数字键2", "Alpha2"],
    ["数字键3", "Alpha3"],
    ["数字键4", "Alpha4"],
    ["数字键5", "Alpha5"],
    ["数字键6", "Alpha6"],
    ["数字键7", "Alpha7"],
    ["数字键8", "Alpha8"],
    ["数字键9", "Alpha9"],
    [":", "Colon"],
    [";", "Semicolon"],
    ["<", "Less"],
    ["=", "Equals"],
    [">", "Greater"],
    ["?", "Question"],
    ["@", "At"],
    ["[", "LeftBracket"],
    ["\\", "Backslash"],
    ["]", "RightBracket"],
    ["^", "Caret"],
    ["_", "Underscore"],
    ["`", "BackQuote"],
    ["A", "A"],
    ["B", "B"],
    ["C", "C"],
    ["D", "D"],
    ["E", "E"],
    ["F", "F"],
    ["G", "G"],
    ["H", "H"],
    ["I", "I"],
    ["J", "J"],
    ["K", "K"],
    ["L", "L"],
    ["M", "M"],
    ["N", "N"],
    ["O", "O"],
    ["P", "P"],
    ["Q", "Q"],
    ["R", "R"],
    ["S", "S"],
    ["T", "T"],
    ["U", "U"],
    ["V", "V"],
    ["W", "W"],
    ["X", "X"],
    ["Y", "Y"],
    ["Z", "Z"],
    ["Delete", "Delete"],
    ["小键盘0", "Keypad0"],
    ["小键盘1", "Keypad1"],
    ["小键盘2", "Keypad2"],
    ["小键盘3", "Keypad3"],
    ["小键盘4", "Keypad4"],
    ["小键盘5", "Keypad5"],
    ["小键盘6", "Keypad6"],
    ["小键盘7", "Keypad7"],
    ["小键盘8", "Keypad8"],
    ["小键盘9", "Keypad9"],
    ["小键盘.", "KeypadPeriod"],
    ["小键盘/", "KeypadDivide"],
    ["小键盘*", "KeypadMultiply"],
    ["小键盘-", "KeypadMinus"],
    ["小键盘+", "KeypadPlus"],
    ["小键盘回车", "KeypadEnter"],
    ["小键盘=", "KeypadEquals"],
    ["方向键上", "UpArrow"],
    ["方向键下", "DownArrow"],
    ["方向键右", "RightArrow"],
    ["方向键左", "LeftArrow"],
    ["Insert", "Insert"],
    ["Home", "Home"],
    ["End", "End"],
    ["PageUp", "PageUp"],
    ["PageDown", "PageDown"],
    ["F1", "F1"],
    ["F2", "F2"],
    ["F3", "F3"],
    ["F4", "F4"],
    ["F5", "F5"],
    ["F6", "F6"],
    ["F7", "F7"],
    ["F8", "F8"],
    ["F9", "F9"],
    ["F10", "F10"],
    ["F11", "F11"],
    ["F12", "F12"],
    ["F13", "F13"],
    ["F14", "F14"],
    ["F15", "F15"],
    ["Numlock", "Numlock"],
    ["CapsLock", "CapsLock"],
    ["ScrollLock", "ScrollLock"],
    ["右Shift", "RightShift"],
    ["左Shift", "LeftShift"],
    ["右Ctrl", "RightControl"],
    ["左Ctrl", "LeftControl"],
    ["右Alt", "RightAlt"],
    ["左Alt", "LeftAlt"],
    ["右Ctrl", "RightCommand"],
    ["左Ctrl", "LeftCommand"],
    ["左Windows键", "LeftWindows"],
    ["右Windows键", "RightWindows"],
    ["AltGr", "AltGr"],
    ["Help", "Help"],
    ["Print", "Print"],
    ["SysReq", "SysReq"],
    ["Break", "Break"],
    ["Menu", "Menu"]
];

Blockly.Msg["unityengine_input_Keyboard_input_mouse_opt"] = [
    ["鼠标左键", "Mouse0"],
    ["鼠标右键", "Mouse1"],
    ["鼠标中键", "Mouse2"],
    ["鼠标第3个按键", "Mouse3"],
    ["鼠标第4个按键", "Mouse4"],
    ["鼠标第5个按键", "Mouse5"],
    ["鼠标第6个按键", "Mouse6"],
];

Blockly.Msg["unityengine_input_Keyboard_input_Joystick_opt"] = [
    ["手柄按键0", "JoystickButton0"],
    ["手柄按键1", "JoystickButton1"],
    ["手柄按键2", "JoystickButton2"],
    ["手柄按键3", "JoystickButton3"],
    ["手柄按键4", "JoystickButton4"],
    ["手柄按键5", "JoystickButton5"],
    ["手柄按键6", "JoystickButton6"],
    ["手柄按键7", "JoystickButton7"],
    ["手柄按键8", "JoystickButton8"],
    ["手柄按键9", "JoystickButton9"],
    ["手柄按键10", "JoystickButton10"],
    ["手柄按键11", "JoystickButton11"],
    ["手柄按键12", "JoystickButton12"],
    ["手柄按键13", "JoystickButton13"],
    ["手柄按键14", "JoystickButton14"],
    ["手柄按键15", "JoystickButton15"],
    ["手柄按键16", "JoystickButton16"],
    ["手柄按键17", "JoystickButton17"],
    ["手柄按键18", "JoystickButton18"],
    ["手柄按键19", "JoystickButton19"],
    ["第一个手柄按键0", "Joystick1Button0"],
    ["第一个手柄按键1", "Joystick1Button1"],
    ["第一个手柄按键2", "Joystick1Button2"],
    ["第一个手柄按键3", "Joystick1Button3"],
    ["第一个手柄按键4", "Joystick1Button4"],
    ["第一个手柄按键5", "Joystick1Button5"],
    ["第一个手柄按键6", "Joystick1Button6"],
    ["第一个手柄按键7", "Joystick1Button7"],
    ["第一个手柄按键8", "Joystick1Button8"],
    ["第一个手柄按键9", "Joystick1Button9"],
    ["第一个手柄按键10", "Joystick1Button10"],
    ["第一个手柄按键11", "Joystick1Button11"],
    ["第一个手柄按键12", "Joystick1Button12"],
    ["第一个手柄按键13", "Joystick1Button13"],
    ["第一个手柄按键14", "Joystick1Button14"],
    ["第一个手柄按键15", "Joystick1Button15"],
    ["第一个手柄按键16", "Joystick1Button16"],
    ["第一个手柄按键17", "Joystick1Button17"],
    ["第一个手柄按键18", "Joystick1Button18"],
    ["第一个手柄按键19", "Joystick1Button19"],
    ["第二个手柄按键0", "Joystick2Button0"],
    ["第二个手柄按键1", "Joystick2Button1"],
    ["第二个手柄按键2", "Joystick2Button2"],
    ["第二个手柄按键3", "Joystick2Button3"],
    ["第二个手柄按键4", "Joystick2Button4"],
    ["第二个手柄按键5", "Joystick2Button5"],
    ["第二个手柄按键6", "Joystick2Button6"],
    ["第二个手柄按键7", "Joystick2Button7"],
    ["第二个手柄按键8", "Joystick2Button8"],
    ["第二个手柄按键9", "Joystick2Button9"],
    ["第二个手柄按键10", "Joystick2Button10"],
    ["第二个手柄按键11", "Joystick2Button11"],
    ["第二个手柄按键12", "Joystick2Button12"],
    ["第二个手柄按键13", "Joystick2Button13"],
    ["第二个手柄按键14", "Joystick2Button14"],
    ["第二个手柄按键15", "Joystick2Button15"],
    ["第二个手柄按键16", "Joystick2Button16"],
    ["第二个手柄按键17", "Joystick2Button17"],
    ["第二个手柄按键18", "Joystick2Button18"],
    ["第二个手柄按键19", "Joystick2Button19"],
    ["第三个手柄按键0", "Joystick3Button0"],
    ["第三个手柄按键1", "Joystick3Button1"],
    ["第三个手柄按键2", "Joystick3Button2"],
    ["第三个手柄按键3", "Joystick3Button3"],
    ["第三个手柄按键4", "Joystick3Button4"],
    ["第三个手柄按键5", "Joystick3Button5"],
    ["第三个手柄按键6", "Joystick3Button6"],
    ["第三个手柄按键7", "Joystick3Button7"],
    ["第三个手柄按键8", "Joystick3Button8"],
    ["第三个手柄按键9", "Joystick3Button9"],
    ["第三个手柄按键10", "Joystick3Button10"],
    ["第三个手柄按键11", "Joystick3Button11"],
    ["第三个手柄按键12", "Joystick3Button12"],
    ["第三个手柄按键13", "Joystick3Button13"],
    ["第三个手柄按键14", "Joystick3Button14"],
    ["第三个手柄按键15", "Joystick3Button15"],
    ["第三个手柄按键16", "Joystick3Button16"],
    ["第三个手柄按键17", "Joystick3Button17"],
    ["第三个手柄按键18", "Joystick3Button18"],
    ["第三个手柄按键19", "Joystick3Button19"],
    ["第四个手柄按键0", "Joystick4Button0"],
    ["第四个手柄按键1", "Joystick4Button1"],
    ["第四个手柄按键2", "Joystick4Button2"],
    ["第四个手柄按键3", "Joystick4Button3"],
    ["第四个手柄按键4", "Joystick4Button4"],
    ["第四个手柄按键5", "Joystick4Button5"],
    ["第四个手柄按键6", "Joystick4Button6"],
    ["第四个手柄按键7", "Joystick4Button7"],
    ["第四个手柄按键8", "Joystick4Button8"],
    ["第四个手柄按键9", "Joystick4Button9"],
    ["第四个手柄按键10", "Joystick4Button10"],
    ["第四个手柄按键11", "Joystick4Button11"],
    ["第四个手柄按键12", "Joystick4Button12"],
    ["第四个手柄按键13", "Joystick4Button13"],
    ["第四个手柄按键14", "Joystick4Button14"],
    ["第四个手柄按键15", "Joystick4Button15"],
    ["第四个手柄按键16", "Joystick4Button16"],
    ["第四个手柄按键17", "Joystick4Button17"],
    ["第四个手柄按键18", "Joystick4Button18"],
    ["第四个手柄按键19", "Joystick4Button19"],
    ["第五个手柄按键0", "Joystick5Button0"],
    ["第五个手柄按键1", "Joystick5Button1"],
    ["第五个手柄按键2", "Joystick5Button2"],
    ["第五个手柄按键3", "Joystick5Button3"],
    ["第五个手柄按键4", "Joystick5Button4"],
    ["第五个手柄按键5", "Joystick5Button5"],
    ["第五个手柄按键6", "Joystick5Button6"],
    ["第五个手柄按键7", "Joystick5Button7"],
    ["第五个手柄按键8", "Joystick5Button8"],
    ["第五个手柄按键9", "Joystick5Button9"],
    ["第五个手柄按键10", "Joystick5Button10"],
    ["第五个手柄按键11", "Joystick5Button11"],
    ["第五个手柄按键12", "Joystick5Button12"],
    ["第五个手柄按键13", "Joystick5Button13"],
    ["第五个手柄按键14", "Joystick5Button14"],
    ["第五个手柄按键15", "Joystick5Button15"],
    ["第五个手柄按键16", "Joystick5Button16"],
    ["第五个手柄按键17", "Joystick5Button17"],
    ["第五个手柄按键18", "Joystick5Button18"],
    ["第五个手柄按键19", "Joystick5Button19"],
    ["第六个手柄按键0", "Joystick6Button0"],
    ["第六个手柄按键1", "Joystick6Button1"],
    ["第六个手柄按键2", "Joystick6Button2"],
    ["第六个手柄按键3", "Joystick6Button3"],
    ["第六个手柄按键4", "Joystick6Button4"],
    ["第六个手柄按键5", "Joystick6Button5"],
    ["第六个手柄按键6", "Joystick6Button6"],
    ["第六个手柄按键7", "Joystick6Button7"],
    ["第六个手柄按键8", "Joystick6Button8"],
    ["第六个手柄按键9", "Joystick6Button9"],
    ["第六个手柄按键10", "Joystick6Button10"],
    ["第六个手柄按键11", "Joystick6Button11"],
    ["第六个手柄按键12", "Joystick6Button12"],
    ["第六个手柄按键13", "Joystick6Button13"],
    ["第六个手柄按键14", "Joystick6Button14"],
    ["第六个手柄按键15", "Joystick6Button15"],
    ["第六个手柄按键16", "Joystick6Button16"],
    ["第六个手柄按键17", "Joystick6Button17"],
    ["第六个手柄按键18", "Joystick6Button18"],
    ["第六个手柄按键19", "Joystick6Button19"],
    ["第七个手柄按键0", "Joystick7Button0"],
    ["第七个手柄按键1", "Joystick7Button1"],
    ["第七个手柄按键2", "Joystick7Button2"],
    ["第七个手柄按键3", "Joystick7Button3"],
    ["第七个手柄按键4", "Joystick7Button4"],
    ["第七个手柄按键5", "Joystick7Button5"],
    ["第七个手柄按键6", "Joystick7Button6"],
    ["第七个手柄按键7", "Joystick7Button7"],
    ["第七个手柄按键8", "Joystick7Button8"],
    ["第七个手柄按键9", "Joystick7Button9"],
    ["第七个手柄按键10", "Joystick7Button10"],
    ["第七个手柄按键11", "Joystick7Button11"],
    ["第七个手柄按键12", "Joystick7Button12"],
    ["第七个手柄按键13", "Joystick7Button13"],
    ["第七个手柄按键14", "Joystick7Button14"],
    ["第七个手柄按键15", "Joystick7Button15"],
    ["第七个手柄按键16", "Joystick7Button16"],
    ["第七个手柄按键17", "Joystick7Button17"],
    ["第七个手柄按键18", "Joystick7Button18"],
    ["第七个手柄按键19", "Joystick7Button19"],
    ["第八个手柄按键0", "Joystick8Button0"],
    ["第八个手柄按键1", "Joystick8Button1"],
    ["第八个手柄按键2", "Joystick8Button2"],
    ["第八个手柄按键3", "Joystick8Button3"],
    ["第八个手柄按键4", "Joystick8Button4"],
    ["第八个手柄按键5", "Joystick8Button5"],
    ["第八个手柄按键6", "Joystick8Button6"],
    ["第八个手柄按键7", "Joystick8Button7"],
    ["第八个手柄按键8", "Joystick8Button8"],
    ["第八个手柄按键9", "Joystick8Button9"],
    ["第八个手柄按键10", "Joystick8Button10"],
    ["第八个手柄按键11", "Joystick8Button11"],
    ["第八个手柄按键12", "Joystick8Button12"],
    ["第八个手柄按键13", "Joystick8Button13"],
    ["第八个手柄按键14", "Joystick8Button14"],
    ["第八个手柄按键15", "Joystick8Button15"],
    ["第八个手柄按键16", "Joystick8Button16"],
    ["第八个手柄按键17", "Joystick8Button17"],
    ["第八个手柄按键18", "Joystick8Button18"],
    ["第八个手柄按键19", "Joystick8Button19"]
];
Blockly.Msg["unityengine_input_keyboard_input_STATE"] = [
    ["按下", "Down"],
    ["按住", "Held"],
    ["抬起", "Up"]
];



Blockly.Msg["AstarPath.BlockUntilCalculated"] = "AstarPath.BlockUntilCalculated A %1 B";
Blockly.Msg["Datetime_compare_opt"] = [
    [
        "=",
        "="
    ],
    [
        "!=",
        "!="
    ],
    [
        ">",
        ">"
    ],
    [
        ">=",
        ">="
    ],
    [
        "<",
        "<"
    ],
    [
        "<=",
        "<="
    ]
];

Blockly.Msg['DuplicatedEvent'] = "重复监听事件";


//Dotween 开始
Blockly.Msg["OpenBlock.UDOTween.Gameobject_recttransform_do_anchor_position_single_and_message"] = "%1 矩形变换在 %2 秒内将 %5 移动到 %3 完成后发送 %6 消息 附加信息 %4";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_recttransform_do_anchor_position_single_and_wait"] = "%1 矩形变换在 %2 秒内将 %4 移动到 %3 [完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_recttransform_do_change_scale_and_wait"] = "%1 矩形变换在 %2 秒内将缩放比例改为 %3 [完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_recttransform_do_change_scale_and_message"] = "%1 矩形变换在 %2 秒内将缩放比例改为 %3 ,完成后发送 %5消息并附加信息 %4";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_shake_position_and_message"] = "%1 在 %2 方向偏移 %3 角度 %4 震动 %5 次 完成后发送 %7 消息并附加信息 %6";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_recttransform_do_anchor_position_and_message"] = "%1 矩形变换在 %2 秒内移动到 %3 完成后发送 %5 消息 附加信息 %4";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_recttransform_do_change_size_and_wait"] = "%1 矩形变换在 %2 秒内将长宽改为 %3 [完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Gameobject_recttransform_do_change_size_and_message"] = "%1 矩形变换在 %2 秒内将长宽改为 %3 完成后发送 %5 消息 附加信息 %4";
Blockly.Msg["OpenBlock.UDOTween.AudioSource_DoFade_and_message"] = "%1 音频组件 %3 秒内将音量改为 %2 完成后发送 %5 消息 附加信息 %4";
Blockly.Msg["OpenBlock.UDOTween.AudioSource_DoFade_and_wait"] = "%1 音频组件 %3 秒内将音量改为 %2 [完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.AudioSource_DoPitch_and_message"] = "%1 音频组件 %3 秒内将音调改为 %2 完成后发送 %5 消息 附加信息 %4";
Blockly.Msg["OpenBlock.UDOTween.AudioSource_DoPitch_and_wait"] = "%1 音频组件 %3 秒内将音调改为 %2 [完成后继续]";
//三维变换-移动
Blockly.Msg["OpenBlock.UDOTween.Transfrom_DoMove_and_wait"] = "%1 的%2坐标在 %4秒内移动到%3,整数变化%5[完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Transfrom_DoMove_and_message"] = "%1 的%2坐标在 %4秒内移动到%3,整数变化%5,完成后发送 %6消息 附加信息%7";
Blockly.Msg["OpenBlock.UDOTween.Transfrom_DoMoveSingle_and_wait"] = "%1的%2%3坐标在 %5秒内移动到%4,整数变化%6[完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Transfrom_DoMoveSingle_and_message"] = "%1的%2%3坐标 在 %5秒内移动到%4,整数变化%6,完成后发送 %7消息 附加信息%8";
//物理
Blockly.Msg["OpenBlock.UDOTween.Rigidbody_DoMove_and_wait"] = "%1的刚体在 %3秒内移动到%2,整数变化%4[完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Rigidbody_DoMove_and_message"] = "%1的刚体 在 %3秒内移动到%2,整数变化%4,完成后发送 %5消息 附加信息%6";
Blockly.Msg["OpenBlock.UDOTween.Rigidbody2D_DoMove_and_wait"] = "%1的2D刚体 在 %3秒内移动到%2,整数变化%4[完成后继续]";
Blockly.Msg["OpenBlock.UDOTween.Rigidbody2D_DoMove_and_message"] = "%1的2D刚体 在 %3秒内移动到%2,整数变化%4,完成后发送 %5消息 附加信息%6";
Blockly.Msg["OpenBlock.UDOTween.Rigidbody_DoJump_and_message"] = "%1的刚体在%5秒内移动到%2,跳跃%4次，最大高度%3，整数变化%6,完成后发送 %7消息 附加信息%8";
Blockly.Msg["OpenBlock.UDOTween.Rigidbody2D_DoJump_and_message"] = "%1的2D刚体在%5秒内移动到%2,跳跃%4次，最大高度%3，整数变化%6,完成后发送 %7消息 附加信息%8";




//UMeshCombineStudio 开始
Blockly.Msg["OpenBlock.UMeshCombineStudio.AddObjectAutomatically"] = "%1MCS组件更新新对象";
Blockly.Msg["OpenBlock.UMeshCombineStudio.CombineAll"] = "%1MCS组件重新调整";

//UMobileNotification
Blockly.Msg["OpenBlock.UMobileNotification.Notification_Send"] = "推送消息标题%1内容%2时间%3";
Blockly.Msg["OpenBlock.UMobileNotification.Notification_Init"] = "初始化推送消息系统并清空计划推送的消息";




























//颜色相关
//块颜色
//H:0-360
//S:36-44
//B:67-72

//灰色
// for (let themeName in Blockly.Themes) {
//     let theme = Blockly.Themes[themeName]
//     theme.setAllBlockStyles({
//         "control_blocks": {
//             "colourPrimary": "#a5745b"
//         },
//         "operation_blocks": {
//             "colourPrimary": "#5ba567"
//         },
//         "message_blocks": {
//             "colourPrimary": "#5b6ba5"
//         },
//         "event_blocks": {
//             "colourPrimary": "#675ba5"
//         },
//         "string_blocks": {
//             "colourPrimary": "#5ba58c"
//         },
//         "list_blocks": {
//             "colourPrimary": "#5263ab"
//         },
//         "array_blocks": {
//             "colourPrimary": "#0fBD8C"
//         },
//         "gameobject_blocks": {
//             "colourPrimary": "#a55b5b"
//         },
//         "component_blocks": {
//             "colourPrimary": "#a58d5b"
//         },
//         "canvas_blocks": {
//             "colourPrimary": "#a56c5b"
//         },
//         "physical_blocks": {
//             "colourPrimary": "#6eab8e"
//         },
//         "time_blocks": {
//             "colourPrimary": "#5ba5a2"
//         },
//         "scene_blocks": {
//             "colourPrimary": "#7ba4e2"
//         },
//         "mobile_blocks": {
//             "colourPrimary": "#ab6e8b"
//         },
//         "output_blocks": {
//             "colourPrimary": "#6faaa0"
//         },
//         "input_blocks": {
//             "colourPrimary": "#6eab7f"
//         },
//         "screen_blocks": {
//             "colourPrimary": "#d38c54"
//         },
//         "application_blocks": {
//             "colourPrimary": "#ab6e6e"
//         },
//         "audio_blocks": {
//             "colourPrimary": "#575E75"
//         },
//         "date_blocks": {
//             "colourPrimary": "#8c67b7"
//         },
//         "LoadAssets_blocks": {
//             "colourPrimary": "#54b357"
//         },
//         "textmeshpro_blocks": {
//             "colourPrimary": "#a16c38"
//         },
//         "network_blocks": {
//             "colourPrimary": "#6e95ab"
//         },
//         "thirdparty_blocks": {
//             "colourPrimary": "#a06eab"
//         },
//         "camera_blocks": {
//             "colourPrimary": "#1cb479"
//         },
//         "database_blocks": {
//             "colourPrimary": "#83bb5a"
//         },
//         "rank_blocks": {
//             "colourPrimary": "#cbb643"
//         },
//         "diytype_blocks": {
//             "colourPrimary": "#cb8943"
//         },
//         "colour_blocks": {
//             "colourPrimary": "20"
//         },
//         "logic_blocks": {
//             "colourPrimary": "#a5745b"
//         },
//         "loop_blocks": {
//             "colourPrimary": "#a5745b"
//         },
//         "math_blocks": {
//             "colourPrimary": "#5ba567"
//         },
//         "navmesh_blocks": {
//             "colourPrimary": "728fb3"
//         },
//         "procedure_blocks": {
//             "colourPrimary": "290"
//         },
//         "text_blocks": {
//             "colourPrimary": "#5ba58c"
//         },
//         "variable_blocks": {
//             "colourPrimary": "330"
//         },
//         "variable_dynamic_blocks": {
//             "colourPrimary": "310"
//         },
//         "hat_blocks": {
//             "colourPrimary": "330",
//             "hat": "cap"
//         },
//         "shadow_blocks": {
//             "colourPrimary": "#404040"
//         },

//     })


//分组颜色-灰
// theme.setCategoryStyle("control_category", {
//     "colour": "#a5745b"
// })
// theme.setCategoryStyle("navmesh_category", {
//     "colour": "#728fb3"
// })
// theme.setCategoryStyle("operation_category", {
//     "colour": "#5ba567"
// })
// theme.setCategoryStyle("message_category", {
//     "colour": "#5b6ba5"
// })
// theme.setCategoryStyle("string_category", {
//     "colour": "#5ba58c"
// })
// theme.setCategoryStyle("list_category", {
//     "colour": "#5263ab"
// })
// theme.setCategoryStyle("event_category", {
//     "colour": "#675ba5"
// })
// theme.setCategoryStyle("array_category", {
//     "colour": "#0fBD8C"
// })
// theme.setCategoryStyle("gameobject_category", {
//     "colour": "#a55b5b"
// })
// theme.setCategoryStyle("component_category", {
//     "colour": "#a58d5b"
// })
// theme.setCategoryStyle("canvas_category", {
//     "colour": "#a56c5b"
// })
// theme.setCategoryStyle("physical_category", {
//     "colour": "#6eab8e"
// })
// theme.setCategoryStyle("time_category", {
//     "colour": "#5ba5a2"
// })
// theme.setCategoryStyle("scene_category", {
//     "colour": "#7ba4e2"
// })
// theme.setCategoryStyle("mobile_category", {
//     "colour": "#ab6e8b"
// })
// theme.setCategoryStyle("output_category", {
//     "colour": "#6faaa0"
// })
// theme.setCategoryStyle("input_category", {
//     "colour": "#6eab7f"
// })
// theme.setCategoryStyle("screen_category", {
//     "colour": "#d38c54"
// })
// theme.setCategoryStyle("application_category", {
//     "colour": "#ab6e6e"
// })
// theme.setCategoryStyle("audio_category", {
//     "colour": "#575E75"
// })
// theme.setCategoryStyle("date_category", {
//     "colour": "#8c67b7"
// })
// theme.setCategoryStyle("LoadAssets_category", {
//     "colour": "#54b357"
// })
// theme.setCategoryStyle("textmeshpro_category", {
//     "colour": "#a16c38"
// })
// theme.setCategoryStyle("network_category", {
//     "colour": "#6e95ab"
// })
// theme.setCategoryStyle("thirdparty_category", {
//     "colour": "#a06eab"
// })
// theme.setCategoryStyle("camera_category", {
//     "colour": "#1cb479"
// })
// theme.setCategoryStyle("database_category", {
//     "colour": "#83bb5a"
// })
// theme.setCategoryStyle("rank_category", {
//     "colour": "#cbb643"
// })
// theme.setCategoryStyle("diytype_category", {
//     "colour": "#cb8943"
// })
// theme.setCategoryStyle("colour_category", {
//     "colour": "20"
// })
// theme.setCategoryStyle("logic_category", {
//     "colour": "210"
// })
// theme.setCategoryStyle("loop_category", {
//     "colour": "120"
// })
// theme.setCategoryStyle("math_category", {
//     "colour": "#5ba567"
// })
// theme.setCategoryStyle("procedure_category", {
//     "colour": "290"
// })
// theme.setCategoryStyle("text_category", {
//     "colour": "160"
// })
// theme.setCategoryStyle("variable_category", {
//     "colour": "330"
// })
// theme.setCategoryStyle("variable_dynamic_category", {
//     "colour": "280"
// })
// theme.setCategoryStyle("variable_diy_category", {
//     "colour": "#5ba5a2"
// })
// theme.setCategoryStyle("variable_state_category", {
//     "colour": "#9966FF"
// })
// theme.setCategoryStyle("parameter_category", {
//     "colour": "#bf634b"
// })

//
//黑色
//预留颜色、547eac、629c1e、9c8c1e、ad4f4f
OpenBlock.setAllBlockStyles({
    "native_call": {
        "colourPrimary": "#1e9c45"
    },
    "control_blocks": {
        "colourPrimary": "#a5745b"
    },
    "operation_blocks": {
        "colourPrimary": "#47868a"
    },
    "message_blocks": {
        "colourPrimary": "#5b6ba5"
    },
    "event_blocks": {
        "colourPrimary": "#a55b5b"
    },
    "string_blocks": {
        "colourPrimary": "#44904a"
    },
    "list_blocks": {
        "colourPrimary": "#5263ab"
    },
    "array_blocks": {
        "colourPrimary": "#0fBD8C"
    },
    "gameobject_blocks": {
        "colourPrimary": "#1e9c45"
    },
    "component_blocks": {
        "colourPrimary": "#9e8144"
    },
    "canvas_blocks": {
        "colourPrimary": "#a56c5b"
    },
    "physical_blocks": {
        "colourPrimary": "#8a6b22"
    },
    "time_blocks": {
        "colourPrimary": "#257976"
    },
    "scene_blocks": {
        "colourPrimary": "#944d67"
    },
    "mobile_blocks": {
        "colourPrimary": "#ab6e8b"
    },
    "output_blocks": {
        "colourPrimary": "#3863a3"
    },
    "input_blocks": {
        "colourPrimary": "#3f8c80"
    },
    "screen_blocks": {
        "colourPrimary": "#bc7e4d"
    },
    "application_blocks": {
        "colourPrimary": "#ab6e6e"
    },
    "audio_blocks": {
        "colourPrimary": "#575E75"
    },
    "date_blocks": {
        "colourPrimary": "#825aa6"
    },
    "LoadAssets_blocks": {
        "colourPrimary": "#408a49"
    },
    "textmeshpro_blocks": {
        "colourPrimary": "#a16c38"
    },
    "network_blocks": {
        "colourPrimary": "#a05aa6"
    },
    "thirdparty_blocks": {
        "colourPrimary": "#8b644b"
    },
    "camera_blocks": {
        "colourPrimary": "#1cb479"
    },
    "database_blocks": {
        "colourPrimary": "#625eaa"
    },
    "rank_blocks": {
        "colourPrimary": "#956732"
    },
    "struct_blocks": {
        "colourPrimary": "#3c8170"
    },
    "colour_blocks": {
        "colourPrimary": "20"
    },
    "logic_blocks": {
        "colourPrimary": "#a5745b"
    },
    "loop_blocks": {
        "colourPrimary": "#a5745b"
    },
    "math_blocks": {
        "colourPrimary": "#47868a"
    },
    "navmesh_blocks": {
        "colourPrimary": "#327e74"
    },
    "procedure_blocks": {
        "colourPrimary": "#375a7d"
    },
    "text_blocks": {
        "colourPrimary": "#44904a"
    },
    "variable_blocks": {
        "colourPrimary": "#377d5b"
    },
    "variable_dynamic_blocks": {
        "colourPrimary": "#7f4c5f"
    },
    "variable_diy_blocks": {
        "colourPrimary": "#71377d"
    },
    "hat_blocks": {
        "colourPrimary": "330",
        "hat": "cap"
    },

    "shadow_blocks": {
        "colourPrimary": "#404040"
    },
})
//分组颜色-黑

OpenBlock.setCategoryStyle("control_category", {
    "colour": "#a5745b"
})
OpenBlock.setCategoryStyle("native_call_category", {
    "colour": "#1e9c45"
})
OpenBlock.setCategoryStyle("navmesh_category", {
    "colour": "#327e74"
})
OpenBlock.setCategoryStyle("operation_category", {
    "colour": "#47868a"
})
OpenBlock.setCategoryStyle("message_category", {
    "colour": "#5b6ba5"
})
OpenBlock.setCategoryStyle("string_category", {
    "colour": "#44904a"
})
OpenBlock.setCategoryStyle("list_category", {
    "colour": "#5263ab"
})
OpenBlock.setCategoryStyle("event_category", {//1d7835
    "colour": "#a55b5b"
})
OpenBlock.setCategoryStyle("array_category", {
    "colour": "#0fBD8C"
})
OpenBlock.setCategoryStyle("gameobject_category", {
    "colour": "#388757"
})
OpenBlock.setCategoryStyle("component_category", {
    "colour": "#9e8144"
})
OpenBlock.setCategoryStyle("canvas_category", {
    "colour": "#a56c5b"
})
OpenBlock.setCategoryStyle("physical_category", {
    "colour": "#8a6b22"
})
OpenBlock.setCategoryStyle("time_category", {
    "colour": "#257976"
})
OpenBlock.setCategoryStyle("scene_category", {
    "colour": "#944d67"
})
OpenBlock.setCategoryStyle("mobile_category", {
    "colour": "#ab6e8b"
})
OpenBlock.setCategoryStyle("output_category", {
    "colour": "#3863a3"
})
OpenBlock.setCategoryStyle("input_category", {
    "colour": "#3f8c80"
})
OpenBlock.setCategoryStyle("screen_category", {
    "colour": "#bc7e4d"
})
OpenBlock.setCategoryStyle("application_category", {
    "colour": "#ab6e6e"
})
OpenBlock.setCategoryStyle("audio_category", {
    "colour": "#575E75"
})
OpenBlock.setCategoryStyle("date_category", {
    "colour": "#825aa6"
})
OpenBlock.setCategoryStyle("LoadAssets_category", {
    "colour": "#408a49"
})
OpenBlock.setCategoryStyle("textmeshpro_category", {
    "colour": "#a16c38"
})
OpenBlock.setCategoryStyle("network_category", {
    "colour": "#a05aa6"
})
OpenBlock.setCategoryStyle("thirdparty_category", {
    "colour": "#8b644b"
})
OpenBlock.setCategoryStyle("camera_category", {
    "colour": "#1cb479"
})
OpenBlock.setCategoryStyle("database_category", {
    "colour": "#625eaa"
})
OpenBlock.setCategoryStyle("rank_category", {
    "colour": "#3c8170"
})
OpenBlock.setCategoryStyle("diytype_category", {
    "colour": "#518377"
})
OpenBlock.setCategoryStyle("colour_category", {
    "colour": "20"
})
OpenBlock.setCategoryStyle("logic_category", {
    "colour": "210"
})
OpenBlock.setCategoryStyle("loop_category", {
    "colour": "120"
})
OpenBlock.setCategoryStyle("math_category", {
    "colour": "#47868a"
})
OpenBlock.setCategoryStyle("procedure_category", {
    "colour": "375a7d"
})
OpenBlock.setCategoryStyle("text_category", {
    "colour": "160"
})
OpenBlock.setCategoryStyle("variable_category", {
    "colour": "377d5b"
})
OpenBlock.setCategoryStyle("variable_dynamic_category", {
    "colour": "7f4c5f"
})
OpenBlock.setCategoryStyle("variable_diy_category", {
    "colour": "#71377d"
})
OpenBlock.setCategoryStyle("variable_state_category", {
    "colour": "#7d3737"
})
OpenBlock.setCategoryStyle("parameter_category", {
    "colour": "#3c377d"
})