/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
(function () {
    let JSPreviewer;
    let JSPreviewer_iframe;
    const ROOT_URL = "../jsruntime/test/";
    const PAGE_URL = "index.html";
    Vue.component('jspreviewer', {
        props: [],
        data() {
            return {
                enable: true
            };
        },
        template: '\
    <modal id="jspreviewer" @on-visible-change="onVisibleChange" :mask-closable="false" :mask="false"\
     footer-hide draggable v-model="enable"  title="预览器" width="375"\
     closable="false" >\
    <iframe ref="jsprevieweriframe" id="jsprevieweriframe" scrolling="no"\
     src="' + ROOT_URL + PAGE_URL + '"\
     style="width:375px;height:640px;border:0;">\
    </frame>\
    </modal>',
        mounted() {
            JSPreviewer = this;
            JSPreviewer_iframe = this.$refs.jsprevieweriframe;
        },
        methods: {
            onVisibleChange(v) {
                if (!v) {
                    JSPreviewer = null;
                    JSPreviewer_iframe = null;
                    UB_IDE.removeSubwindowComponent('jspreviewer');
                }
            }
        }
    });
    OpenBlock.onInited(() => {
        class Simulator extends OBConnector {
            pageUrl;
            /**
             * @type {Window}
             */
            constructor() {
                super();
                this.pageUrl = "../jsruntime/test/index.html";
            }
            loadConfig() {
                let env = ROOT_URL + 'env/'
                let jsarr = [
                    env + 'i18n_zh.js',
                    env + 'nativeEvent.js',
                    env + 'native.js',
                ];
                var xmlpath = env + "nativeBlocks.xml";
                OpenBlock.loadNativeInfo(jsarr, xmlpath);
            }
            runProject() {
                OpenBlock.exportExePackage((err, result) => {
                    if (!err) {
                        let runProjectCmd = { "cmd": "runProject", "bytes": result, fsm: "Start.Main" };
                        if (!JSPreviewer_iframe) {
                            UB_IDE.ensureSubwindowComponent('jspreviewer');
                            setTimeout(() => {
                                JSPreviewer_iframe.contentWindow.window.onload = () => {
                                    JSPreviewer_iframe.contentWindow.postMessage(runProjectCmd);
                                };
                            }, 0);
                        } else {
                            JSPreviewer_iframe.contentWindow.postMessage(runProjectCmd);
                        }
                    }
                });
            }
        }
        window.Simulator = Simulator;
    });
})();