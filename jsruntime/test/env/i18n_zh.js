/*jshint -W117 */
Blockly.Msg["touchstart"] = "触摸按下";
Blockly.Msg["touchmove"] = "触摸移动";
Blockly.Msg["touchcancel"] = "触摸取消";
Blockly.Msg["touchend"] = "触摸停止";
Blockly.Msg["click"] = "点击";
Blockly.Msg["longpress"] = "长按";
Blockly.Msg["swipe"] = "快速滑动";

Blockly.Msg["width"] = "宽";
Blockly.Msg["height"] = "高";
Blockly.Msg["Canvas2d_fillRect"] = "绘制实心矩形";
Blockly.Msg["Canvas2d_strokeRect"] = "绘制描边矩形";
Blockly.Msg["Canvas2d_clearRect"] = "清空矩形区域";
Blockly.Msg["Canvas2d_setFillStyleColor"] = "设置填充颜色";
Blockly.Msg["Canvas2d_setStrokeStyleColor"] = "设置描边颜色";
Blockly.Msg["Canvas2d_fillText"] = "绘制实心文字";
Blockly.Msg["Canvas2d_beginPath"] = "开始路径";
Blockly.Msg["Canvas2d_arc"] = "创建弧形";
Blockly.Msg["Canvas2d_fill"] = "填充路径";
Blockly.Msg["Canvas2d_stroke"] = "描边路径";
Blockly.Msg["Canvas2d_closePath"] = "关闭路径";
Blockly.Msg["Canvas2d_setFont"] = "设置字体";
Blockly.Msg["Canvas2d_getFont"] = "当前字体";
Blockly.Msg["Canvas2d_arcTo"] = "依据圆弧经过的点和圆弧半径创建圆弧路径";
Blockly.Msg["Canvas2d_bezierCurveTo"] = "创建三次贝赛尔曲线路径";
Blockly.Msg["Canvas2d_moveTo"] = "移动到指定点";
Blockly.Msg["Canvas2d_lineTo"] = "到指定点进行路径连接";
Blockly.Msg["Canvas2d_setTextAlign"] = "文本绘制中的文本对齐方式";
Blockly.Msg["Canvas2d_getTextAlign"] = "文本绘制中的文本对齐方式";
Blockly.Msg["Canvas2d_setTextAlign_options"] = [
    "left", "right", "center", "start", "end"
];
Blockly.Msg["left"] = "左";
Blockly.Msg["right"] = "右";
Blockly.Msg["center"] = "中";
Blockly.Msg["start"] = "开始";
Blockly.Msg["end"] = "结束";
Blockly.Msg["Canvas2d_setTextBaseline"] = "文字垂直方向的对齐方式";
Blockly.Msg["Canvas2d_getTextBaseline"] = "文字垂直方向的对齐方式";
Blockly.Msg["Canvas2d_setTextBaseline_options"] = [
    "alphabetic", "top", "hanging", "middle", "ideographic", "bottom"
];
Blockly.Msg["alphabetic"] = "字母基线";
Blockly.Msg["top"] = "顶部";
Blockly.Msg["hanging"] = "悬挂";
Blockly.Msg["middle"] = "中间";
Blockly.Msg["ideographic"] = "表意字基线";
Blockly.Msg["bottom"] = "底部";
Blockly.Msg["Canvas2d_ellipse"] = "创建椭圆路径";
Blockly.Msg["Canvas2d_rect"] = "创建矩形路径";
Blockly.Msg["Canvas2d_rotate"] = "顺时针旋转(弧度)";
Blockly.Msg["Canvas2d_scale"] = "缩放倍数";
Blockly.Msg["keydown"] = "按下按键";
Blockly.Msg["keyup"] = "抬起按键";
Blockly.Msg["Canvas2d_height"] = "画板高度";
Blockly.Msg["Canvas2d_width"] = "画板宽度";