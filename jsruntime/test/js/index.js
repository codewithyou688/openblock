/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

import * as obvm from '../../runtime/vm.js'
import * as obcanvaslib from './canvas.js'


/**
 * @type {HTMLCanvasElement}
 */
let stage = document.getElementById("stage");

stage.height = parseInt(window.getComputedStyle(stage).height);
stage.width = parseInt(window.getComputedStyle(stage).width);
let obcanvas = new obcanvaslib.OBCanvas2D(stage);
let nativeLibs = [obcanvas.install.bind(obcanvas)];
var scriptArrayBuffer;
var phaserSceneJSONStr;
let loadedScript;
let vm;
let fsm;
function v2(e) {
    let t = e.currentTarget;
    let height = t.height;
    let width = t.width;
    let x = e.clientX;
    let y = e.clientY;
    let swidth = t.getBoundingClientRect().width;
    let sheight = t.getBoundingClientRect().height;
    let sx = x / swidth * width;
    let sy = y / sheight * height;
    sx = Math.floor(sx);
    sy = Math.floor(sy);
    return new obcanvaslib.Vector2(sx, sy);
}
function ob_event(name, argType, arg) {
    if (vm) {
        vm.BroadcastMessage(new obvm.OBEventMessage(name, argType, arg, null));
    }
}
stage.addEventListener('touchstart', (e) => {
    ob_event('touchstart', 'Vector2', v2(e));
}, false);
stage.addEventListener('mousedown', (e) => {
    ob_event('touchstart', 'Vector2', v2(e));
}, false);
stage.addEventListener('touchmove', (e) => { ob_event('touchmove', 'Vector2', v2(e)) }, false);
stage.addEventListener('touchcancel', (e) => { ob_event('touchcancel', 'Vector2', v2(e)) }, false);
stage.addEventListener('touchend', (e) => { ob_event('touchend', 'Vector2', v2(e)) }, false);
stage.addEventListener('mouseup', (e) => { ob_event('touchend', 'Vector2', v2(e)) }, false);
stage.addEventListener('click', (e) => { ob_event('click', 'Vector2', v2(e)) }, false);
stage.addEventListener('longpress', (e) => { ob_event('longpress', 'Vector2', v2(e)) }, false);
stage.addEventListener('swipe', () => { ob_event('swipe') }, false);
document.addEventListener('keydown', (e) => {
    ob_event('keydown', 'String', e.key)
}, false);
document.addEventListener('keyup', (e) => {
    ob_event('keyup', 'String', e.key)
}, false);
const inputElement = document.getElementById("input_script");
inputElement.addEventListener("change", () => {
    const fileList = inputElement.files;
    if (fileList.length == 0) {
        return;
    }
    let reader = new FileReader();
    reader.onload = (evt) => {
        stage.width = stage.width;
        scriptArrayBuffer = reader.result;
        loadedScript = obvm.OBScriptLoader.loadScript(scriptArrayBuffer, nativeLibs);
    };
    reader.readAsArrayBuffer(fileList[0]);
}, false);

const runButton = document.getElementById('button_run')
runButton.onclick = function () {
    if (vm) {
        vm = null;
    }
    if (loadedScript) {
        vm = new obvm.OBVM(loadedScript);
        // vm.Output = alert.bind(window);
        let fsmname = document.getElementById("input_start_fsm").value;
        fsm = vm.CreateFSM(fsmname);
        if (!fsm) {
            throw Error("No FSM named " + fsmname);
        }
    }
};

function step() {
    window.requestAnimationFrame(step);
    if (vm) {
        vm.update();
    }
}
window.requestAnimationFrame(step);

if (window.parent) {
    let messageHandler = {
        runProject(evt) {
            stage.width = stage.width;
            scriptArrayBuffer = evt.data.bytes;
            loadedScript = obvm.OBScriptLoader.loadScript(scriptArrayBuffer, nativeLibs);
            vm = new obvm.OBVM(loadedScript);
            // vm.Output = alert.bind(window);
            let fsmname = evt.data.fsm || document.getElementById("input_start_fsm").value;
            fsm = vm.CreateFSM(fsmname);
            if (!fsm) {
                throw Error("No FSM named " + fsmname);
            }
        }
    };
    window.addEventListener("message", receiveMessage);

    function receiveMessage(event) {
        let cmd = event.data.cmd;
        if (messageHandler[cmd]) {
            messageHandler[cmd](event);
        }
    }
}